#include "catch.hpp"

#include "../include/rezultat.h"

#include <sstream>
#include <iostream>

#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>



TEST_CASE("Dodavanje igraca")
{
    Rezultat rezultat;
    Igrac igrac("Marko");
    SECTION("Novi igrac"){
        REQUIRE(!rezultat.postojiIgrac(igrac.getIme()));
        rezultat.dodajIgraca(igrac);
        REQUIRE(rezultat.postojiIgrac(igrac.getIme()));
    }

    SECTION("Igrac vec postoji"){
        rezultat.dodajIgraca(igrac);
        int size = rezultat.getIgraci().size();
        REQUIRE(rezultat.postojiIgrac(igrac.getIme()));
        rezultat.dodajIgraca(igrac);
        int newSize = rezultat.getIgraci().size();
        REQUIRE(size == newSize);


    }


}


TEST_CASE("Provera da li postoji igrac")
{
    Rezultat rezultat;
    Igrac igrac("Marko");
    rezultat.dodajIgraca(igrac);

    SECTION("Igrac postoji u rezultatima"){
        REQUIRE(rezultat.postojiIgrac(igrac.getIme()));
    }

    SECTION("Igrac ne postoji u rezultatima"){
        REQUIRE_FALSE(rezultat.postojiIgrac("Nepostojeci igrac"));
    }

    SECTION("Dobijamo igraca sa trazenim imenom"){
        REQUIRE(rezultat.postojiIgrac(igrac.getIme()));
        Igrac dohvaceniIgrac = rezultat.nadjiIgraca(igrac.getIme());
        REQUIRE(dohvaceniIgrac.getIme() == igrac.getIme());
    }
    SECTION("Dobijamo igraca sa praznim imenom kada prosledjeni igrac ne postoji"){
        Igrac nepostojeciIgrac("Zarko");
        REQUIRE(!rezultat.postojiIgrac(nepostojeciIgrac.getIme()));
        Igrac dohvaceniIgrac = rezultat.nadjiIgraca(nepostojeciIgrac.getIme());
        REQUIRE(dohvaceniIgrac.getIme() == "");
    }
}


TEST_CASE("Azuriranje igraca") {
    Rezultat rezultat;
    Igrac igrac1("Marko");
    Igrac igrac2("Zarko");
    rezultat.dodajIgraca(igrac1);
    rezultat.dodajIgraca(igrac2);

    Igrac azuriranIgrac("azuriraniZarko");
    rezultat.azurirajIgraca(igrac2.getIme(), azuriranIgrac);

    QVector<Igrac> igraci = rezultat.getIgraci();
    REQUIRE(igraci.size() == 2);
    REQUIRE(igraci[0].getIme() == "Marko");
    REQUIRE(igraci[1].getIme() == "azuriraniZarko");

}


TEST_CASE("Serijalizacija"){

    Rezultat rezultat;
    Igrac igrac1("Marko");
    Igrac igrac2("Zarko");
    rezultat.dodajIgraca(igrac1);
    rezultat.dodajIgraca(igrac2);

    const QString testFileName = "test_file.json";



    SECTION("Igraci se cuvaju u dati json file"){
        rezultat.sacuvajIgraceJson(testFileName);

        QFile file(testFileName);
        REQUIRE(file.exists());

        if(file.exists())
        {
            if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                QByteArray fileContent = file.readAll();
                QJsonDocument jsonDocument = QJsonDocument::fromJson(fileContent);

                QJsonObject jsonObject = jsonDocument.object();

                QJsonValue playersValue = jsonObject["players"];

                QJsonArray playerArray = playersValue.toArray();
                int condition = 0;
                for (const QJsonValue& playerValue : playerArray) {
                    QJsonObject playerObject = playerValue.toObject();
                    REQUIRE(playerObject.contains("ime"));

                    QJsonValue nameValue = playerObject["ime"];
                    QString playerName = nameValue.toString();

                    if (playerName == "Marko" or playerName == "Zarko")
                        condition++;

                }

                file.close();
                REQUIRE(condition == 2);
            }
            else {
                FAIL("Neuspeh u otvaranju fajla za citanje");
            }
        }
        else {
            FAIL("Fajl ne postoji");
        }

    }

    SECTION("Prethodno sacuvani igraci se mogu ucitati"){
        rezultat.sacuvajIgraceJson(testFileName);

        QFile file(testFileName);
        REQUIRE(file.exists());

        if(file.exists())
        {
            rezultat.ucitajIgraceJson(testFileName);
            int condition = 0;
            for(const Igrac& igrac : rezultat.getIgraci()){
                if(igrac.getIme() == "Marko" || igrac.getIme() == "Zarko")
                    condition++;
            }

            REQUIRE(condition == 2);
        }
        else {
            FAIL("Fajl ne postoji");
        }
    }

    SECTION("Azurira se jedan od prethodno sacuvanih igraca i promene se cuvaju"){
        rezultat.sacuvajIgraceJson(testFileName);
        QFile file(testFileName);
        REQUIRE(file.exists());

        Igrac azurirani("azuriraniZarko");
        rezultat.azurirajIgracaJson(testFileName, igrac2.getIme(), azurirani);


        if(file.exists())
        {
            rezultat.ucitajIgraceJson(testFileName);
            int condition = 0;
            for(const Igrac& igrac: rezultat.getIgraci())
            {
                if(igrac.getIme() == "Marko" || igrac.getIme() == "azuriraniZarko")
                    condition++;
                else if(igrac.getIme() == "Zarko"){
                    condition = 0;
                    break;
                }
            }

            REQUIRE(condition == 2);
        }
        else
        {
            FAIL("Fajl ne postoji");

        }

    }

}








