#include "catch.hpp"
#include "../include/sveci.h"
#include "../include/timer.h"
#include "../forms/ui_sveci.h"
#include <QApplication>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QSignalSpy>

TEST_CASE("Provera ispravnosti unosa u lineEdit", "[provera_unosa]") {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    Timer* testTimer = new Timer();

    
    Sveci* sveci = new Sveci(testTimer, 0, nullptr);



    SECTION("Provera konstruktora i inicijalizacije") {
       
        testTimer->start();

        REQUIRE(sveci->minimumWidth() == 1280);
        REQUIRE(sveci->minimumHeight() == 720);
    }

   SECTION("Provera ispravnosti unosa") {
        // Testiranje funkcije proveri()
       int condition = 0;
       int inicijalniBrojWidzeta = sveci->ui->horizontalLayout->count();
       sveci->ui->lineEdit->setText("SAVA");
       sveci->proveri();
       if(sveci->ui->horizontalLayout->count() > inicijalniBrojWidzeta)
           condition = 1;
        REQUIRE(condition == 1);


    }
   SECTION("Provera neispravnosti unosa") {
       // Testiranje funkcije proveri()
       int condition = 0;
       int inicijalniBrojWidzeta = sveci->ui->horizontalLayout->count();
       sveci->ui->lineEdit->setText("Nepostojeci unos");
       sveci->proveri();
       if(sveci->ui->horizontalLayout->count() == inicijalniBrojWidzeta && sveci->ui->lineEdit->text() == "")
           condition = 1;
       REQUIRE(condition == 1);


   }
    delete sveci;
    delete testTimer;
}
