#include <trompeloeil.hpp>

#include "catch.hpp"
#include "../include/timer.h"
#include "../include/Challenge_test.h"
#include "../include/challengeMorzeovaAzbukaUI.h"
#include <QApplication>
#include <QSignalSpy>
#include <QDebug>
#include <QLineEdit>
#include <thread>
#include <chrono>
#include <QTimer>
#include <QTest>
#include <QObject>




class MockChallenge : public ChallengeMorzeovaAzbukaUI
{
    public:
        MAKE_MOCK0(spelujRec, void());
        MockChallenge(Timer *vreme, int pocetnoVreme, QWidget *parent) : ChallengeMorzeovaAzbukaUI(vreme, pocetnoVreme, parent)
        {}

        
};


TEST_CASE("Mrozeova azbuka izazov", "[class]")
{

    int argc = 0;
    char* argv[] = { nullptr };

    QApplication app(argc, argv);

    SECTION("Reagovanje na signale od sata i ispravno postavljanje pocetnih vrednosti polja")
    {

        Timer timer;
        timer.start();
        ChallengeMorzeovaAzbukaUI challenge(&timer,0,nullptr);

        QSignalSpy spy(&timer, SIGNAL(azurirajVreme(int)));

        emit timer.azurirajVreme(1);

        app.processEvents();


        REQUIRE(spy.count() > 0);
        REQUIRE(challenge.getRec() == "staklo");

    }

    SECTION("Klikom na dugme za paljenje svetla se pokrece funkcija spelujRec")
    {
        Timer timer;
        timer.start();
        MockChallenge challenge(&timer,0,nullptr);

        REQUIRE_CALL(challenge, spelujRec()).TIMES(1);

        QObject::connect(challenge.ui->findChild<QPushButton*>("pbPonovi"), &QPushButton::clicked, &challenge, &MockChallenge::spelujRec);
        //challenge.spelujRec();
        challenge.ui->findChild<QPushButton*>("pbPonovi")->click();

        //app.processEvents();
        QTest::qWait(100);

        REQUIRE(challenge.getCurrentIndex() == 0);
  

    }

    SECTION("Crtanje pozadine kada istekne tajmer kao simulacija paljenja i gasenja svetla"){
        Timer timer;
        timer.start();
        ChallengeMorzeovaAzbukaUI challenge(&timer,0,nullptr);

        challenge.ui->findChild<QPushButton*>("pbPonovi")->click();

        std::this_thread::sleep_for(std::chrono::milliseconds(400));

        app.processEvents();

        REQUIRE(challenge.getImagePath() == ":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_upaljeno.png");
        REQUIRE(challenge.getCurrentIndex() == 1);


    }


}
