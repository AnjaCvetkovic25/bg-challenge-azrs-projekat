// zeleznickastanica.h

#ifndef ZELEZNICKASTANICA_H
#define ZELEZNICKASTANICA_H

#include "Challenge_test.h"
#include "timer.h"
#include <QObject>
#include <QPushButton>
#include <QWidget>

class ZeleznickaStanica : public Challenge_test
{
	Q_OBJECT
	public:
	ZeleznickaStanica(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~ZeleznickaStanica();

	public slots:
	void pomoc();

	private slots:
	void levoLjubljana_clicked();
	void desnoLjubljana_clicked();

	void levoMokraGora_clicked();
	void desnoMokraGora_clicked();

	void levoTito_clicked();
	void desnoTito_clicked();

	void levoZagreb_clicked();
	void desnoZagreb_clicked();

	void btnProveri_clicked();

	void Tekstovi();

	private:
	QList<QString> odgovori;
	QPushButton* tito;
	QPushButton* mokraGora;
	QPushButton* zagreb;
	QPushButton* ljubljana;

	int* brojacTito;
	int* brojacMokraGora;
	int* brojacZagreb;
	int* brojacLjubljana;

	QWidget* ui = nullptr;
};

#endif // ZELEZNICKASTANICA_H
