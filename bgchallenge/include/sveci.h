#ifndef SVECI_H
#define SVECI_H
#include "Challenge_test.h"
#include "timer.h"

#include <QLabel>
#include <QPaintEvent>
#include <QPushButton>
#include <QWidget>

namespace Ui
{
class Sveci;
}

class Sveci : public Challenge_test
{
	Q_OBJECT

	public:
	explicit Sveci(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~Sveci();
	Ui::Sveci* ui;

	public slots:
	void Tekstovi();
	void proveri();
	void proveri2();
	void proveri3();
	void proveri4();
	void proveri5();
	void proveri6();
	void F();
	void R();
	void E();
	void S();
	void K();
	void A();

	private:
	QPushButton* button1;
	QPushButton* button2;
	QPushButton* button3;
	QPushButton* button4;
	QPushButton* button5;
	QPushButton* button6;

	QTimer m_timer;

	QLabel* labela;
};

#endif // SVECI_H
