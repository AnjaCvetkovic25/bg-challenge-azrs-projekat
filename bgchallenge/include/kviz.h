#ifndef KVIZ_H
#define KVIZ_H

#include "Challenge_test.h"
#include "timer.h"
#include <QStackedWidget>

namespace Ui
{
class kviz;
}

class kviz : public Challenge_test
{
	Q_OBJECT

	public:
	explicit kviz(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~kviz();
	void setUpBackground(const QString& path);

	void Tekstovi();
	void predjiDalje();
	bool ponovi;
	bool isCorrect;

	private:
	friend class KvizTest;
	Ui::kviz* ui;

	private slots:
	friend class KvizTest;
	void onPitanje1OdgovorClicked();
	void onPitanje2OdgovorClicked();
	void onPitanje3OdgovorClicked();
	void onPitanje4OdgovorClicked();
};

#endif // KVIZ_H