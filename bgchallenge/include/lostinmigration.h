#ifndef LOSTINMIGRATION_H
#define LOSTINMIGRATION_H

#include "Challenge_test.h"
#include <QObject>
#include <QWidget>

#include "timer.h"
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSequentialAnimationGroup>
#include <QTextBrowser>
#include <QWidget>

class LostInMigration : public Challenge_test
{
	Q_OBJECT
	public:
	LostInMigration(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~LostInMigration();
	void Tekstovi();
	QPushButton* getBtnLevo() const
	{
		return btnLevo;
	}
	QPushButton* getBtnDesno() const
	{
		return btnDesno;
	}
	QPushButton* getBtnGore() const
	{
		return btnGore;
	}
	QPushButton* getBtnDole() const
	{
		return btnDole;
	}
	QWidget* getUi() const
	{
		return ui;
	}

	QLineEdit* getLeResenjeIzazova() const
	{
		return leResenjeIzazova;
	}
	QLabel* getLblUnesiResenje() const
	{
		return lblUnesiResenje;
	}

	void startFadeDownAnimations();
	public slots:

	private slots:
	void kliknuoDugmeLevo(int* brojac, QString* kombinacija, QString ispravnaKombinacija);
	void kliknuoDugmeGore(int* brojac, QString* kombinacija, QString ispravnaKombinacija);
	void kliknuoDugmeDesno(int* brojac, QString* kombinacija, QString ispravnaKombinacija);
	void kliknuoDugmeDole(int* brojac, QString* kombinacija, QString ispravnaKombinacija);

	void hideFrame(QLabel* label, QString slovo);
	void fadeDownLabel(QLabel* label, QSequentialAnimationGroup* group, int startDelay, QString slovo);

	void pomoc();

	signals:

	private:
	QString kombinacija;
	int brojac;
	QString ispravnaKombinacija;
	QString resenjeIzazova;

	QWidget* ui = nullptr;

	QPushButton* btnLevo;
	QPushButton* btnDesno;
	QPushButton* btnGore;
	QPushButton* btnDole;

	QLineEdit* leResenjeIzazova;
	QLabel* lblUnesiResenje;

	QLabel* lblDesno1;
	QLabel* lblDesno2;
	QLabel* lblDole1;
	QLabel* lblDesno3;
	QLabel* lblGore1;
	QLabel* lblLevo1;
	QLabel* lblGore2;
	QLabel* lblLevo2;
};
#endif // LOSTINMIGRATION_H
