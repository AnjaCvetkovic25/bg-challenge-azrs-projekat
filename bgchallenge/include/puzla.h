#ifndef PUZLA_H
#define PUZLA_H

#include "Challenge_test.h"
#include "time.h"
#include <QWidget>

namespace Ui
{
class Puzla;
}

class Puzla : public Challenge_test
{
	Q_OBJECT

	public:
	explicit Puzla(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~Puzla();
	void Tekstovi();
	Ui::Puzla* ui;

	public slots:
	void onPbSlika1Clicked();
	void onPbSlika2Cilcked();
	void onPbSlika3Clicked();
	void obPbSlika4Clicked();
	void onPbSlika5Clicked();
	void onPbSlika6Clicked();
	void onPbV1Clicked();
	void onPbMissingPieceClicked();

	// private:
};

#endif // PUZLA_H
