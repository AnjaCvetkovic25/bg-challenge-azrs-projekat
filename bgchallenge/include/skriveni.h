#ifndef SKRIVENI_H
#define SKRIVENI_H

#include "Challenge_test.h"
#include "timer.h"
#include <QLabel>
#include <QObject>
#include <QPushButton>
#include <QTextBrowser>
#include <QWidget>

class Skriveni : public Challenge_test
{
	Q_OBJECT
	public:
	Skriveni(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~Skriveni();

	public slots:

	void Tekstovi();
	void pomoc();

	void btnVucic_clicked();
	void btnMeta_clicked();
	void btnMegafon_clicked();
	void btnTransparent_clicked();
	void btnSuzavac_clicked();
	void btnFerari_clicked();
	void btnNasaoVucica_clicked();
	void btnNasaoMetu_clicked();
	void btnNasaoZastavu_clicked();
	void btnNasaoTransparent_clicked();
	void btnNasaoSuzavac_clicked();
	void btnNasaoMegafon_clicked();

	private:
	bool nasaoVucica = false;
	bool nasaoZastavu = false;
	bool nasaoSuzavac = false;
	bool nasaoMegafon = false;
	bool nasaoMetu = false;
	bool nasaoTransparent = false;

	QTextBrowser* tbOpis;

	QPushButton* btnNasaoMegafon;
	QPushButton* btnNasaoMetu;
	QPushButton* btnNasaoSuzavac;
	QPushButton* btnNasaoTransparent;
	QPushButton* btnNasaoVucica;
	QPushButton* btnNasaoZastavu;

	QPushButton* btnMegafon;
	QPushButton* btnMeta;
	QPushButton* btnSuzavac;
	QPushButton* btnTransparent;
	QPushButton* btnVucic;
	QPushButton* btnFerari;

	int* brojac;

	QWidget* ui = nullptr;
};

#endif // SKRIVENI_H
