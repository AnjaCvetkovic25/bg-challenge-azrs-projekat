#ifndef MEMORIJA_H
#define MEMORIJA_H

#include "Challenge_test.h"
#include "timer.h"
#include <QGraphicsOpacityEffect>
#include <QLineEdit>
#include <QList>
#include <QObject>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QTimer>
#include <QWidget>

class Memorija : public Challenge_test
{
	Q_OBJECT
	public:
	Memorija(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~Memorija();
	void Tekstovi();
	void Onesposobi();
	void Osposobi();
	void onBtnClicked();
	void handleTimeoutNetacno();
	void handleTimeoutTacno();
	QList<QPushButton*> getButtonPar() const
	{
		return buttonPar;
	}
	public slots:

	void pomoc();

	private:
	QList<QPushButton*> buttonList;
	QList<QPushButton*> buttonPar;
	bool animationInProgress = false;
	QTimer* timerNetacno;
	QTimer* timerTacno;
	int* brojac;
	QString sifra;

	QPushButton* btnCard1;
	QPushButton* btnCard2;
	QPushButton* btnCard3;
	QPushButton* btnCard4;
	QPushButton* btnCard5;
	QPushButton* btnCard6;
	QPushButton* btnCard7;
	QPushButton* btnCard8;
	QPushButton* btnCard9;
	QPushButton* btnCard10;
	QPushButton* btnCard11;
	QPushButton* btnCard12;
	QPushButton* btnCard13;
	QPushButton* btnCard14;
	QPushButton* btnCard15;
	QPushButton* btnCard16;
	QPushButton* btnCard17;
	QPushButton* btnCard18;
	QPushButton* btnCard19;
	QPushButton* btnCard20;

	QLineEdit* leResenjeIzazova;
	QTextBrowser* tbUputstvo;

	QGraphicsOpacityEffect* mEffect0 = nullptr;
	QGraphicsOpacityEffect* mEffect1 = nullptr;
	QPropertyAnimation* animation1 = nullptr;
	QPropertyAnimation* opacityAnimation1 = nullptr;
	QPropertyAnimation* animation2 = nullptr;
	QPropertyAnimation* opacityAnimation2 = nullptr;
	QParallelAnimationGroup* group = nullptr;

	QWidget* ui = nullptr;
};

#endif // MEMORIJA_H
