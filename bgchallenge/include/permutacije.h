#ifndef PERMUTACIJE_H
#define PERMUTACIJE_H

#include "Challenge_test.h"
#include "timer.h"
#include <QLabel>
#include <QObject>
#include <QPushButton>
#include <QTextBrowser>
#include <QWidget>

class Permutacije : public Challenge_test
{
	Q_OBJECT

	public:
	Permutacije(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~Permutacije();

	public slots:

	void Tekstovi();
	void pomoc();

	private slots:
	void kliknuoTrougao();
	void kliknuoZvezdu();
	void kliknuoKrug();
	void kliknuoReset();
	void PoklopljenaSlova();

	private:
	QString kombinacija;
	QString resenje;
	QString prethodniKlik;

	QLabel* lblPocetna;
	QLabel* lblStambol;
	QPushButton* btnTrougao;
	QPushButton* btnZvezda;
	QPushButton* btnKrug;
	QPushButton* btnReset;

	QTextBrowser* tbUputstvo;

	QWidget* ui;
};

#endif // PERMUTACIJE_H
