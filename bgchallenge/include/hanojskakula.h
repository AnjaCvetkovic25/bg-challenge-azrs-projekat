#ifndef HANOJSKAKULA_H
#define HANOJSKAKULA_H

#include "../forms/ui_hanojskakula.h"
#include "Challenge_test.h"
#include "timer.h"
#include <QDialog>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainter>
#include <QStack>

namespace Ui
{
class Hanojskakula;
}
class Hanojskakula : public Challenge_test
{
	Q_OBJECT

	public:
	explicit Hanojskakula(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~Hanojskakula();
	void zapocniChallenge() override;
	void zavrsiChallenge() override;
	void postaviPocetnoStanje();
	bool eventFilter(QObject* watched, QEvent* event) override;
	void obradiDropDiska(QGraphicsProxyWidget& proxyIzvorniStub, QGraphicsProxyWidget& proxyCiljniStub,
						 QPointF inicijalnaPozicijaDiska);
	void postaviDiskNaStub(QGraphicsPixmapItem* disk, QStack<unsigned char>& stek, QLabel* ciljniStub, int yOffset);
	QStack<unsigned char>& vratiOdgovarajuciStek(QGraphicsProxyWidget* proxyCiljniStub);
	bool dozvoljenPotez(QGraphicsPixmapItem* draggedDisk, QStack<unsigned char> stub);
	void smanjiStek(QStack<unsigned char>& stub);
	void promeniDozvolu(QStack<unsigned char> stub, bool dozvola);
	void prikaziPorukuPobede();
	void Tekstovi();
	bool pomocExecuted;

	private slots:
	friend class HanojskakulaTest;
	void on_RetryClicked();
	void pomoc();

	private:
	friend class HanojskakulaTest;
	Ui::Hanojskakula* ui;
	QGraphicsView* viewHanojskeKule;
	QGraphicsScene* sceneHanojskeKule;
	QGraphicsPixmapItem* izabranDisk = nullptr;
	QGraphicsPixmapItem* disk1Item;
	QGraphicsPixmapItem* disk2Item;
	QGraphicsPixmapItem* disk3Item;
	QGraphicsProxyWidget* proxyStub1;
	QGraphicsProxyWidget* proxyStub2;
	QGraphicsProxyWidget* proxyStub3;
	QStack<unsigned char> stub1;
	QStack<unsigned char> stub2;
	QStack<unsigned char> stub3;
	QPointF inicijalnaPozicijaDiska = QPointF();
	QGraphicsProxyWidget* proxyInicijalniStub = nullptr;
};

#endif // HANOJSKAKULA_H
