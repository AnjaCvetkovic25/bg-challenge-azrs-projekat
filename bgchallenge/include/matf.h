#ifndef MATF_H
#define MATF_H

#include "../include/Challenge_test.h"
#include "../include/timer.h"
#include <QCoreApplication>
#include <QDialog>
#include <QMessageBox>
#include <QPixmap>
#include <QPushButton>
#include <QTableWidgetItem>
#include <QTimer>
#include <QVector>
#include <QWidget>

namespace Ui
{
class MATF;
}

class MATF : public Challenge_test
{
	Q_OBJECT

	public:
	explicit MATF(Timer* timer, int pocetnoVreme, QWidget* parent = nullptr);
	~MATF();
	void Tekstovi();
	Ui::MATF* ui;
	//    QVector<QVector<int>> *matrica;

	bool getCheckSum(const int& a, const int& b, const int& c);
	//    bool getCheckMatrix(const QVector<QVector<int>> &m);

	public slots:
	void onPbHintClicked();
	void onPbProveriClicked();

	private:
	bool checkSum(const int& a, const int& b, const int& c);
	bool checkMatrix(const QVector<QVector<int>>& m);
};

#endif // MATF_H
