#ifndef CHALLENGEMORZEOVAAZBUKAUI_H
#define CHALLENGEMORZEOVAAZBUKAUI_H
#include "Challenge_test.h"
#include "timer.h"
#include <QLCDNumber>
#include <QPaintEvent>
#include <QPushButton>
#include <QString>
#include <QWidget>

class ChallengeMorzeovaAzbukaUI : public Challenge_test
{
	Q_OBJECT

	public:
	ChallengeMorzeovaAzbukaUI(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~ChallengeMorzeovaAzbukaUI();
	void Tekstovi();

	QWidget* ui = nullptr;

	QString getRec();
	QString getKod();

	QTimer& getTimer();

	int getCurrentIndex();
	QString getImagePath();

	public slots:

	void pogodi(const QString& r);
	void spelujRec();
	void timerExpired();

	private:
	const QString m_rec = "staklo";
	const QString m_kod = ". . .#-#. -#- . -#. - . .#- - -";
	bool upaljenoSvetlo;
	QTimer m_timer;
	int m_currentIndex = 0;
};

#endif // CHALLENGEMORZEOVAAZBUKAUI_H
