#ifndef MAPA_H
#define MAPA_H

#include "../include/Challenge_test.h"
#include "../include/igrac.h"
#include "../include/rezultat.h"
#include "../include/timer.h"
#include <QVector>

class Mapa : public QObject
{
	Q_OBJECT

	public:
	// konstruktor za novog igraca
	Mapa(Igrac& trenutniIgrac, Rezultat* rez, QObject* parent = nullptr);
	~Mapa();
	void sacuvajRezultat(qint64 id);

	void zapocniIgru();

	void zavrsiIgru();

	Timer* m_timer = nullptr;

	int getGameTime();

	public slots:
	void azurirajGameTime(qint64 vreme);
	void resenIzazov(qint64 id);

	signals:
	void azurirajLCDNumber(qint64 vreme);
	void otkljucajNaredniIzazov(qint64 id);
	void cestitajIgracu();

	private:
	Igrac m_trenutniIgrac;
	int m_gameTime;
	Rezultat* m_rez;
};

#endif // MAPA_H
