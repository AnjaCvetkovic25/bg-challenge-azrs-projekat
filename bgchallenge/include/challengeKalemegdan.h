#ifndef CHALLENGEKALEMEGDAN_H
#define CHALLENGEKALEMEGDAN_H
#include "Challenge_test.h"
#include "timer.h"
#include <QLabel>
#include <QPaintEvent>
#include <QPushButton>
#include <QStringList>
#include <QVector>
#include <QWidget>

class ChallengeKalemegdan : public Challenge_test
{
	Q_OBJECT
	public:
	ChallengeKalemegdan(Timer* timer, int pocetnoVreme, QWidget* parent = nullptr);
	virtual void zavrsiChallenge() override;
	void Tekstovi();
	~ChallengeKalemegdan();

	void setTrenutnaPozicija(int poz);
	void setPozicijaPocetnogSlova(int poz);

	int getTrenutnaPozicija();
	int getPozicijaPocetnogSlova();

	QStringList getTrenutnaRec();
	void setTrenutnaRec(const QStringList& rec);

	QVector<QLabel*>& getPolja();

	QWidget* ui;

	public slots:
	void pogodi();
	void izbrisi();
	virtual void zapocniChallenge() override;
	signals:
	void pogodjenaRec();

	protected:
	private:
	QStringList m_rec;
	QVector<QLabel*> m_polja;
	int m_trenutnaPozicija;
	int m_pozicijaPocetnogSlova;
	QStringList generisiRec();
};

#endif // CHALLENGEKALEMEGDAN_H
