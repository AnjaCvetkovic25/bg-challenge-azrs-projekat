#ifndef KLJUCEVI_H
#define KLJUCEVI_H
#include "Challenge_test.h"
#include "timer.h"

#include <QAudioOutput>
#include <QGraphicsPixmapItem>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QMessageBox>
#include <QWidget>

namespace Ui
{
class Kljucevi;
}

class Kljucevi : public Challenge_test
{
	Q_OBJECT

	public:
	explicit Kljucevi(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~Kljucevi();

	protected:
	bool eventFilter(QObject* watched, QEvent* event) override;
	// void paintEvent(QPaintEvent *event) override;
	void zvukVrata();

	public slots:
	void Tekstovi();

	private:
	Ui::Kljucevi* ui;
	QMediaPlayer* player;
	QAudioOutput* audioOutput;
	QGraphicsPixmapItem* izabranaSlika;
	QPointF inicijalnaPozicijaSlike;
	QGraphicsPixmapItem* fiksnaSlika1;
	QGraphicsPixmapItem* fiksnaSlika2;
	QGraphicsPixmapItem* fiksnaSlika3;
	QGraphicsPixmapItem* fiksnaSlika4;
	QGraphicsPixmapItem* pokretnaSlika1;
	QGraphicsPixmapItem* pokretnaSlika2;
	QGraphicsPixmapItem* pokretnaSlika3;
	QGraphicsPixmapItem* pokretnaSlika4;
	int brojKljuceva;
	QTimer m_timer;
	// QString imagePath;

	QGraphicsScene* scene;
};

#endif // KLJUCEVI_H
