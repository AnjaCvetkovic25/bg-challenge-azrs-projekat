#ifndef PRONADJIULJEZA_H
#define PRONADJIULJEZA_H

#include "Challenge_test.h"
#include "timer.h"
#include <QDialog>

namespace Ui
{
class PronadjiUljeza;
}

class PronadjiUljeza : public Challenge_test
{
	Q_OBJECT

	public:
	explicit PronadjiUljeza(Timer* vreme, int pocetnoVreme, QWidget* parent = nullptr);
	~PronadjiUljeza();
	bool znakpitanja;
	bool zlatnibokal;
	bool trisesira;
	bool malivrabac;
	bool sesirmoj;

	private slots:
	friend class PronadjiUljezaTest;
	void znakpitanja_clicked();
	void zlatnibokal_clicked();
	void trisesira_clicked();
	void malivrabac_clicked();
	void sesirmoj_clicked();
	void limunzut_clicked();

	public:
	void Tekstovi();
	void Info();
	void buttonMask(QPushButton* button, const QPixmap& image);

	private:
	friend class PronadjiUljezaTest;
	Ui::PronadjiUljeza* ui;
};

#endif // PRONADJIULJEZA_H
