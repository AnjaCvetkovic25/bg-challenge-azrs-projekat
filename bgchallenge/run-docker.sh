#!/bin/bash

docker run \
  -it --network=host \
  -e DISPLAY=$DISPLAY -e DBUS_SESSION_BUS_ADDRESS \
  -e XAUTHORITY=/root/.Xauthority \
  -v $HOME/.Xauthority:/root/.Xauthority \
  -v /tmp/.X11-unix:/tmp/.X11-unix -v /run:/run \
  --user="$(id --user):$(id --group)" \
  --security-opt apparmor=unconfined \
  -v /dev/sr0:/dev/sr0 \
  -v /dev/cdrom:/dev/cdrom \
  --privileged \
  $1 

