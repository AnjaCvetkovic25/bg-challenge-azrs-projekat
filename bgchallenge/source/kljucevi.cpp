#include "../include/kljucevi.h"
#include "../forms/ui_kljucevi.h"
#include <QGraphicsOpacityEffect>
#include <QGraphicsSceneMouseEvent>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMovie>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QRandomGenerator>
#include <QString>
#include <QTimer>
#include <iostream>

Kljucevi::Kljucevi(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(9, 3, vreme, pocetnoVreme, parent), ui(new Ui::Kljucevi), izabranaSlika(nullptr), brojKljuceva(4)
{
	ui->setupUi(this);
	// this->setStyleSheet("background-color: #333333;");
	player = new QMediaPlayer();
	audioOutput = new QAudioOutput();
	player->setAudioOutput(audioOutput);
	player->setSource(QUrl("qrc:/new/kljucevi/resources/dorm-door-opening-6038.mp3"));
	audioOutput->setVolume(10);

	setImagePath(":/new/pozadine/resources/moskva_bg.png");
	scene = new QGraphicsScene(this);
	scene->setSceneRect(0, 0, 400, 400);

	Tekstovi();
	connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		ui->lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	// Dodavanje fiksne slike(vrata)
	fiksnaSlika1 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
	fiksnaSlika1->setPos(400, 2);
	fiksnaSlika1->setScale(1.5);

	fiksnaSlika2 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
	fiksnaSlika2->setPos(150, 2);
	fiksnaSlika2->setScale(1.5);

	fiksnaSlika3 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
	fiksnaSlika3->setPos(-100, 2);
	fiksnaSlika3->setScale(1.5);

	fiksnaSlika4 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/vrataZ.png"));
	fiksnaSlika4->setPos(-350, 2);
	fiksnaSlika4->setScale(1.5);

	// Dodavanje pokretne slike(kljuc)
	pokretnaSlika1 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
	pokretnaSlika1->setFlag(QGraphicsItem::ItemIsMovable);
	pokretnaSlika1->setPos(400, 350);

	pokretnaSlika2 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
	pokretnaSlika2->setFlag(QGraphicsItem::ItemIsMovable);
	pokretnaSlika2->setPos(150, 350);

	pokretnaSlika3 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
	pokretnaSlika3->setFlag(QGraphicsItem::ItemIsMovable);
	pokretnaSlika3->setPos(-100, 350);

	pokretnaSlika4 = new QGraphicsPixmapItem(QPixmap(":/new/resources/resources/kljucc.png"));
	pokretnaSlika4->setFlag(QGraphicsItem::ItemIsMovable);
	pokretnaSlika4->setPos(-350, 350);

	// Dodavanje na scenu
	scene->addItem(fiksnaSlika1);
	scene->addItem(fiksnaSlika2);
	scene->addItem(fiksnaSlika3);
	scene->addItem(fiksnaSlika4);
	scene->addItem(pokretnaSlika1);
	scene->addItem(pokretnaSlika2);
	scene->addItem(pokretnaSlika3);
	scene->addItem(pokretnaSlika4);

	ui->graphicsView->setScene(scene);
	ui->graphicsView->installEventFilter(this);
	ui->graphicsView->scene()->installEventFilter(this);

	connect(ui->btnNazadNaLokaciju, &QPushButton::clicked, this, &Kljucevi::zatvori);
}

Kljucevi::~Kljucevi()
{
	if (player != nullptr)
	{
		qDebug() << "obrisan player";
		delete player;
		player = nullptr;
	};

	if (audioOutput != nullptr)
	{
		qDebug() << "obrisan audioOutput";
		delete audioOutput;
		audioOutput = nullptr;
	};
	delete ui;
}

bool Kljucevi::eventFilter(QObject* watched, QEvent* event)
{
	if (watched == ui->graphicsView->scene())
	{

		if (event->type() == QEvent::GraphicsSceneMousePress)
		{
			QGraphicsSceneMouseEvent* mouseEvent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
			QGraphicsItem* item =
				ui->graphicsView->scene()->itemAt(mouseEvent->scenePos(), ui->graphicsView->transform());

			if (item && item->type() == QGraphicsPixmapItem::Type && (item->flags() & QGraphicsItem::ItemIsMovable))
			{

				izabranaSlika = qgraphicsitem_cast<QGraphicsPixmapItem*>(item);
				inicijalnaPozicijaSlike = izabranaSlika->pos();
				ui->graphicsView->scene()->setFocusItem(izabranaSlika);
			}
		}
		else if (event->type() == QEvent::GraphicsSceneMouseRelease && izabranaSlika)
		{
			QGraphicsSceneMouseEvent* mouseEvent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
			QGraphicsItem* dropItem =
				ui->graphicsView->scene()->itemAt(mouseEvent->scenePos(), ui->graphicsView->transform());

			if (dropItem && dropItem->type() == QGraphicsPixmapItem::Type &&
				(dropItem->flags() & QGraphicsItem::ItemIsMovable))
			{

				izabranaSlika->setPos(dropItem->pos());
				// Dodaj dodatnu proveru da li je pravi kljuc postavljen iznad pravih
				// vrata
				if (izabranaSlika == pokretnaSlika1 && izabranaSlika->collidesWithItem(fiksnaSlika1))
				{
					// QMessageBox::information(this, "Bravo", "Čestitamo! Otvorili ste
					// vrata!");
					player->play();
					fiksnaSlika1->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
					izabranaSlika->setVisible(false);
					brojKljuceva--;
				}
				else if (izabranaSlika == pokretnaSlika2 && izabranaSlika->collidesWithItem(fiksnaSlika2))
				{
					player->play();
					fiksnaSlika2->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
					izabranaSlika->setVisible(false);
					brojKljuceva--;
				}
				else if (izabranaSlika == pokretnaSlika3 && izabranaSlika->collidesWithItem(fiksnaSlika3))
				{
					player->play();
					fiksnaSlika3->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
					izabranaSlika->setVisible(false);
					brojKljuceva--;
				}
				else if (izabranaSlika == pokretnaSlika4 && izabranaSlika->collidesWithItem(fiksnaSlika4))
				{
					fiksnaSlika4->setPixmap(QPixmap(":/new/resources/resources/vrataO.png"));
					player->play();
					izabranaSlika->setVisible(false);
					brojKljuceva--;
				}
				else
				{

					izabranaSlika->setPos(inicijalnaPozicijaSlike);
				}
			}
			// Otvorena sva vrata
			if (brojKljuceva == 0)
			{
				QMessageBox::information(this, "Bravo", "Čestitamo! Otvorili ste vrata!");
				m_timer.stop();
				// ui->setVisible(false);
				QObjectList children = this->children();
				for (QObject* child : children)
				{
					if (QWidget* widget = qobject_cast<QWidget*>(child))
					{
						widget->setVisible(false);
					}
				}
				QLayout* postojeciLayout = this->layout();
				if (postojeciLayout)
				{
					delete postojeciLayout;
				}
				this->prikaziEdukativniTekst();
				// zavrsiChallenge();
			}

			izabranaSlika = nullptr;
			ui->graphicsView->scene()->setFocusItem(nullptr);
		}
		else if (event->type() == QEvent::GraphicsSceneMouseMove && izabranaSlika)
		{
			QGraphicsSceneMouseEvent* mouseEvent = dynamic_cast<QGraphicsSceneMouseEvent*>(event);
			izabranaSlika->setPos(mouseEvent->scenePos() - izabranaSlika->boundingRect().center());
		}
	}

	return QWidget::eventFilter(watched, event);
}

void Kljucevi::Tekstovi()
{

	m_edukativniTekst = R"(
Hotel Moskva predstavlja jedno od najznačajnijih arhitektonskih dragulja srpske prestonice, izgrađenog u stilu ruske secesije, koji je pod zaštitom države od druge polovine prošlog veka. Tradicija postojanja i poslovanja duža je od jednog veka, učinila je da ovaj hotel poseti preko 45 miliona ljudi.

Pored njene fascinantne istorije, najpoznatija je i po tome što je od trenutka kada je otvorila svoja vrata privukla i oduševila na stotine slavnih ličnosti.
Odmah po otvaranju, 1908. kroz vrata su ušetali izumitelji teorije relativiteta Albert Ajnštajn i njegova žena, naučnica srpskog porekla Mileva Marić. Upravo zbog njihove posete ovom hotelu, predsednički apartman nosi Ajnštajnovo ime. Neki od najpoznatijih svetskih vokala poput Luciana Pavarottija i Reja Carlsa odabrali su baš nju, hotel Moskvu. Džek Nikolson, Robert De Niro, Kirk i Majkl Daglas pridružili su se čitavom nizu glumačkih legendi koje su uživale u raskoši ovog hotela. Takođe, i 37. predsednik Sjedinjenih Američkih Država Ričard Nikson boravio je u hotelu Moskva.
Ipak, najznačajniji od svih slavnih ličnosti jesu književnici koji su hotel Moskvu iz decenije u deceniju činili kulturnim i Književnim centrom. Neka od velikih spisateljskih imena koji krase ovu listu su Jean-Paul Sartre, Orson Welles i Maksim Gorki.

Jedan od prvih gostiju hotela Moskva bio je Branislav Nušić, slavni srpski pisac i redovni gost beogradskih kafana. Stevan Sremac je takođe bio redovan gost ove ustanove. Ovaj slavni srpski akademik, pisac realizma, je navodno prilagodio svoj raspored predavanja u jednoj od beogradskih gimnazija kako bi mogao duže da spava jer je do kasno ostajao u Velikoj Srbiji.
Interesantno je da je hotel Moskva jedini hotel u Beogradu koji nema sobu, a ni apartman pod brojem 13. U toku Drugog svetskog rata, Hotel Moskva je bio glavni štab Gestapoa.

Istoričari koji poznaju probleme srpske duše kažu da su Srbi zavoleli hotel Moskvu jer ih fasada hotela i naziv podsećaju na Rusiju, a unutra je komforno kao u Beču.)";

	m_asocijacija = "Usvaja i menja Ustav";

	m_resenjeAsocijacije.append("skupstina");
	m_resenjeAsocijacije.append("skupština");
	m_resenjeAsocijacije.append("narodna skupstina");
	m_resenjeAsocijacije.append("narodna skupština");
	m_resenjeAsocijacije.append("nardona skupština");

	m_naslovEdukativnogTeksta = "O Hotelu Moskva";

	m_hint = "Čini je 250 narodnih poslanika";
}
