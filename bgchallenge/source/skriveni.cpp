#include "../include/skriveni.h"

#include <QDebug>
#include <QPainter>

#include <QBrush>
#include <QDialog>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QUiLoader>

Skriveni::Skriveni(Timer* vreme, int pocetnoVreme, QWidget* parent) : Challenge_test(10, 3, vreme, pocetnoVreme, parent)
{

	this->setWindowTitle("BgChallenge");
	this->setMinimumWidth(1280);
	this->setMinimumHeight(720);
	this->setFixedSize(this->width(), this->height());

	this->setImagePath(":/new/pozadine/resources/skupstina.png");
	this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

	QFile file(":/new/resources/forms/skriveni2.ui");

	if (!file.open(QFile::ReadOnly))
	{
		qDebug() << "Error opening UI file Skriveni.ui:" << file.errorString();
		return;
	}

	QUiLoader loader;
	ui = loader.load(&file, this);
	ui->setFixedSize(this->width(), this->height());
	file.close();

	QLCDNumber* lcdNumber = ui->findChild<QLCDNumber*>("lcdNumber");

	connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	connect(ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &Skriveni::zatvori);
	connect(ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &Skriveni::pomoc);

	Tekstovi();

	// deklaracija dugmadi
	tbOpis = ui->findChild<QTextBrowser*>("tbOpis");
	qDebug() << tbOpis->objectName() << tbOpis->parent()->objectName();
	btnNasaoMegafon = ui->findChild<QPushButton*>("btnNasaoMegafon");
	btnNasaoMetu = ui->findChild<QPushButton*>("btnNasaoMetu");
	btnNasaoSuzavac = ui->findChild<QPushButton*>("btnNasaoSuzavac");
	btnNasaoTransparent = ui->findChild<QPushButton*>("btnNasaoTransparent");
	btnNasaoVucica = ui->findChild<QPushButton*>("btnNasaoVucica");
	btnNasaoZastavu = ui->findChild<QPushButton*>("btnNasaoZastavu");

	btnMegafon = ui->findChild<QPushButton*>("btnMegafon");
	btnMeta = ui->findChild<QPushButton*>("btnMeta");
	btnSuzavac = ui->findChild<QPushButton*>("btnSuzavac");
	btnTransparent = ui->findChild<QPushButton*>("btnTransparent");
	btnVucic = ui->findChild<QPushButton*>("btnVucic");
	btnFerari = ui->findChild<QPushButton*>("btnFerari");

	tbOpis->setVisible(false);

	QList<QPushButton*> nadjeniObjekti;
	nadjeniObjekti.append(btnNasaoMegafon);
	nadjeniObjekti.append(btnNasaoMetu);
	nadjeniObjekti.append(btnNasaoSuzavac);
	nadjeniObjekti.append(btnNasaoTransparent);
	nadjeniObjekti.append(btnNasaoVucica);
	nadjeniObjekti.append(btnNasaoZastavu);

	// for (QPushButton* btn : nadjeniObjekti) {

	//     btn->setStyleSheet("QPushButton { border: 3px solid black;
	//     border-radius: 10px; background-color: rgba(255, 255, 255, 0.4); }");
	// }

	QList<QPushButton*> skriveniObjekti;
	skriveniObjekti.append(btnMegafon);
	skriveniObjekti.append(btnMeta);
	skriveniObjekti.append(btnSuzavac);
	skriveniObjekti.append(btnTransparent);
	skriveniObjekti.append(btnVucic);
	skriveniObjekti.append(btnFerari);

	connect(btnNasaoMegafon, &QPushButton::clicked, this, &Skriveni::btnNasaoMegafon_clicked);
	connect(btnNasaoMetu, &QPushButton::clicked, this, &Skriveni::btnNasaoMetu_clicked);
	connect(btnNasaoSuzavac, &QPushButton::clicked, this, &Skriveni::btnNasaoSuzavac_clicked);
	connect(btnNasaoTransparent, &QPushButton::clicked, this, &Skriveni::btnNasaoTransparent_clicked);
	connect(btnNasaoVucica, &QPushButton::clicked, this, &Skriveni::btnNasaoVucica_clicked);
	connect(btnNasaoZastavu, &QPushButton::clicked, this, &Skriveni::btnNasaoZastavu_clicked);

	connect(btnMegafon, &QPushButton::clicked, this, &Skriveni::btnMegafon_clicked);
	connect(btnMeta, &QPushButton::clicked, this, &Skriveni::btnMeta_clicked);
	connect(btnSuzavac, &QPushButton::clicked, this, &Skriveni::btnSuzavac_clicked);
	connect(btnTransparent, &QPushButton::clicked, this, &Skriveni::btnTransparent_clicked);
	connect(btnVucic, &QPushButton::clicked, this, &Skriveni::btnVucic_clicked);
	connect(btnFerari, &QPushButton::clicked, this, &Skriveni::btnFerari_clicked);

	brojac = new int(0);
}

Skriveni::~Skriveni()
{
	delete brojac;
	delete ui;
}

void Skriveni::Tekstovi()
{

	m_edukativniTekst = R"(
Prvo je bio put. Rimljani su u prvom veku naše ere trasom današnjeg Bulevara kralja Aleksandra utvrdili glatke kamene ploče u zemljanu podlogu. Carski put je vodio za Atinu.
Po običaju o sahranjivanju, groblje je raslo uz drum kojim se ulazi u grad. Kada je Osmanlijsko carstvo zauzelo Beograd, na mestu rimskog groblja izgradili su Ejnehan-begovu džamiju.
Ni čitavu deceniju kasnije, na brdašcetu pored rimskog druma spaljene su mošti Svetog Save. Plamen sa lomače srpskog svetitelja obasjavao je polumesec na vrhu džamije. Pod njenim minaretom se s vremenom raširilo tursko groblje.
U nekolikim ratovima nalazila se prva na udaru naletnika. Često rušena i sporo obnavljana, prozvana je Batal-džamija - batal na turskom znači napuštena.

Nakon odlaska Turaka Osmanlija, sravnjena je sa zemljom a rusenje su za 230 dukata obavili Cincari.

Prvi projekat skupstine izradio je Konstantin Jovanovic 1891. Ali, zbog politickih dogadjaja i ekonomskih uslova gradnja je odlozena za nekoliko godina i poverena je arhitekti Jovanu Ilkicu.
Istorija je pokazala da je lakše sagraditi palatu Skupštine, nego karakter poslanika. No, da nije sve tako crno, potvrđuje i priča o Stevanu Ćiriću. On je bio novosadski političar, svojevremeno predsednik Skupštine.
I on je smatran zaslužnim za dvorištenje zgrade Skupštine. Da on nije pritiskao da se gradnja ubrza, ko zna šta bi bilo sa sedištem demokratije. Od izrade plana 1892. do useljenja 1936. protekle su 44 godine!
Za to vreme Skupština je zasedala u pivnici u Admirala Geprata, Starom dvoru, u pozorištu u Manješkom parku. Kralj Aleksandar nije mario za Skupštinu, pa ni knez namesnik. Skele bi možda dočekale Drugi svetski rat, a time bi zadugo bilo odloženo dovršenje. Bila bi to batal-skupština.

Zvanican pocetak gradnje palate oznacen je polaganjem kamena temeljca 27. avgusta 1907. u prisustvu kralja Petra I Karadjordjevica i prestolonaslednika Dordja, narodnih poslanika i diplomatskog kora.)";
	m_asocijacija = "Ima li negde u blizini da se zapali sveća?";
	m_hint = "Ako ne znaš ovo, prekrsti se";
	m_resenjeAsocijacije.append("crkva svetog marka");
	m_resenjeAsocijacije.append("crkva sv marka");

	m_naslovEdukativnogTeksta = "O Narodnoj skupštini";
}

void Skriveni::pomoc()
{
	QDialog dijalog;
	dijalog.setFixedHeight(300);
	dijalog.setFixedWidth(400);
	dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
						  "rgb(200,200,200); margin: 10px;}");

	QLabel* text = new QLabel("Skupština je oduvek bilo mesto velikih dešavanja, a naročito velikih -i "
							  "malih- pokušaja da se uradi nešto povodom političkog stanja u našoj "
							  "državi. Svaki protest, bilo da se održavao 1996. ili 2023. godine, imao "
							  "je nešto karakteristično, nešto što opisuje Srbe kao narod, da li to "
							  "bila naša tvrdoglavost, inat, ili možda ludost. Na slici pred tobom, "
							  "sakrivena su 6 simbola protesta u prethodnih dvadesetak godina. Možeš "
							  "li sve da ih pronađeš?",
							  &dijalog);
	text->setAlignment(Qt::AlignCenter);
	text->setWordWrap(true);
	text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
	QPushButton* ok = new QPushButton("OK", &dijalog);
	ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
					  "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
					  "padding: 10px; border-radius: 21px; font: bold 14px;}"
					  "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
					  "#bcbcbc; }"
					  "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
					  "#a0a0a0; }");
	QVBoxLayout* layout = new QVBoxLayout(&dijalog);

	layout->addWidget(text);

	layout->addWidget(ok);
	connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
	dijalog.exec();
}

void Skriveni::btnVucic_clicked()
{
	QPropertyAnimation* buttonAnimation = new QPropertyAnimation(btnVucic, "geometry", this);
	buttonAnimation->setDuration(200);
	buttonAnimation->setStartValue(btnVucic->geometry());
	buttonAnimation->setEndValue(btnVucic->geometry().adjusted(-8, -8, 8, 8));
	buttonAnimation->start();

	btnNasaoVucica->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
								  "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
								  "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
	disconnect(btnVucic, SIGNAL(clicked()), this, SLOT(btnVucic_clicked()));

	nasaoVucica = true;
	(*brojac) += 1;
	if (*brojac == 6)
	{
		QTimer::singleShot(1000, this, [=]() {
			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			// if (this->layout() != nullptr) {
			//     QLayoutItem *item;
			//     while ((item = this->layout()->takeAt(0)) != nullptr) {
			//         qDebug() << "brisem" << item->widget()->objectName() <<
			//         item->widget()->parent()->objectName();

			//         item->widget()->setVisible(false);
			//         delete item;
			//     }
			//     delete this->layout();
			// }

			foreach (QObject* child, this->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			QLayout* postojeciLayout = this->layout();
			if (postojeciLayout)
			{
				delete postojeciLayout;
			}

			prikaziEdukativniTekst();
		});
	}
}

void Skriveni::btnMeta_clicked()
{
	QPropertyAnimation* buttonAnimation = new QPropertyAnimation(btnMeta, "geometry", this);
	buttonAnimation->setDuration(200);
	buttonAnimation->setStartValue(btnMeta->geometry());
	buttonAnimation->setEndValue(btnMeta->geometry().adjusted(-8, -8, 8, 8));
	buttonAnimation->start();

	btnNasaoMetu->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
								"background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
								"#333; padding:10px; border-radius: 21px; font: bold 14px;}");
	disconnect(btnMeta, SIGNAL(clicked()), this, SLOT(btnMeta_clicked()));

	nasaoMetu = true;

	(*brojac) += 1;
	if (*brojac == 6)
	{
		QTimer::singleShot(1000, this, [=]() {
			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			// if (this->layout() != nullptr) {
			//     QLayoutItem *item;
			//     while ((item = this->layout()->takeAt(0)) != nullptr) {
			//         qDebug() << "brisem" << item->widget()->objectName() <<
			//         item->widget()->parent()->objectName();

			//         item->widget()->setVisible(false);
			//         delete item;
			//     }
			//     delete this->layout();
			// }

			foreach (QObject* child, this->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			QLayout* postojeciLayout = this->layout();
			if (postojeciLayout)
			{
				delete postojeciLayout;
			}
			prikaziEdukativniTekst();
		});
	}
}

void Skriveni::btnMegafon_clicked()
{
	QPropertyAnimation* buttonAnimation = new QPropertyAnimation(btnMegafon, "geometry", this);
	buttonAnimation->setDuration(200);
	buttonAnimation->setStartValue(btnMegafon->geometry());
	buttonAnimation->setEndValue(btnMegafon->geometry().adjusted(-8, -8, 8, 8));
	buttonAnimation->start();

	btnNasaoMegafon->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
								   "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
								   "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
	disconnect(btnMegafon, SIGNAL(clicked()), this, SLOT(btnMegafon_clicked()));

	nasaoMegafon = true;

	(*brojac) += 1;
	if (*brojac == 6)
	{
		QTimer::singleShot(1000, this, [=]() {
			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			// if (this->layout() != nullptr) {
			//     QLayoutItem *item;
			//     while ((item = this->layout()->takeAt(0)) != nullptr) {
			//         qDebug() << "brisem" << item->widget()->objectName() <<
			//         item->widget()->parent()->objectName();

			//         item->widget()->setVisible(false);
			//         delete item;
			//     }
			//     delete this->layout();
			// }

			foreach (QObject* child, this->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			QLayout* postojeciLayout = this->layout();
			if (postojeciLayout)
			{
				delete postojeciLayout;
			}
			prikaziEdukativniTekst();
		});
	}
}

void Skriveni::btnTransparent_clicked()
{
	QPropertyAnimation* buttonAnimation = new QPropertyAnimation(btnTransparent, "geometry", this);
	buttonAnimation->setDuration(200);
	buttonAnimation->setStartValue(btnTransparent->geometry());
	buttonAnimation->setEndValue(btnTransparent->geometry().adjusted(-8, -8, 8, 8));
	buttonAnimation->start();

	btnNasaoTransparent->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
									   "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
									   "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
	disconnect(btnTransparent, SIGNAL(clicked()), this, SLOT(btnTransparent_clicked()));

	nasaoTransparent = true;

	(*brojac) += 1;
	if (*brojac == 6)
	{
		QTimer::singleShot(1000, this, [=]() {
			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			// if (this->layout() != nullptr) {
			//     QLayoutItem *item;
			//     while ((item = this->layout()->takeAt(0)) != nullptr) {
			//         qDebug() << "brisem" << item->widget()->objectName() <<
			//         item->widget()->parent()->objectName();

			//         item->widget()->setVisible(false);
			//         delete item;
			//     }
			//     delete this->layout();
			// }

			foreach (QObject* child, this->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			QLayout* postojeciLayout = this->layout();
			if (postojeciLayout)
			{
				delete postojeciLayout;
			}
			prikaziEdukativniTekst();
		});
	}
}

void Skriveni::btnSuzavac_clicked()
{
	QPropertyAnimation* buttonAnimation = new QPropertyAnimation(btnSuzavac, "geometry", this);
	buttonAnimation->setDuration(200);
	buttonAnimation->setStartValue(btnSuzavac->geometry());
	buttonAnimation->setEndValue(btnSuzavac->geometry().adjusted(-8, -8, 8, 8));
	buttonAnimation->start();

	btnNasaoSuzavac->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
								   "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
								   "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
	disconnect(btnSuzavac, SIGNAL(clicked()), this, SLOT(btnSuzavac_clicked()));

	nasaoSuzavac = true;

	(*brojac) += 1;
	if (*brojac == 6)
	{
		QTimer::singleShot(1000, this, [=]() {
			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			// if (this->layout() != nullptr) {
			//     QLayoutItem *item;
			//     while ((item = this->layout()->takeAt(0)) != nullptr) {
			//         qDebug() << "brisem" << item->widget()->objectName() <<
			//         item->widget()->parent()->objectName();

			//         item->widget()->setVisible(false);
			//         delete item;
			//     }
			//     delete this->layout();
			// }

			foreach (QObject* child, this->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			QLayout* postojeciLayout = this->layout();
			if (postojeciLayout)
			{
				delete postojeciLayout;
			}
			prikaziEdukativniTekst();
		});
	}
}

void Skriveni::btnFerari_clicked()
{
	QPropertyAnimation* buttonAnimation = new QPropertyAnimation(btnFerari, "geometry", this);
	buttonAnimation->setDuration(200);
	buttonAnimation->setStartValue(btnFerari->geometry());
	buttonAnimation->setEndValue(btnFerari->geometry().adjusted(-8, -8, 8, 8));
	buttonAnimation->start();

	btnNasaoZastavu->setStyleSheet("QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
								   "background-color: rgb(144,238,144); border: 1px solid #dcdcdc; color: "
								   "#333; padding:10px; border-radius: 21px; font: bold 14px;}");
	disconnect(btnFerari, SIGNAL(clicked()), this, SLOT(btnFerari_clicked()));

	nasaoZastavu = true;

	(*brojac) += 1;
	if (*brojac == 6)
	{
		QTimer::singleShot(1000, this, [=]() {
			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			// if (this->layout() != nullptr) {
			//     QLayoutItem *item;
			//     while ((item = this->layout()->takeAt(0)) != nullptr) {
			//         qDebug() << "brisem" << item->widget()->objectName() <<
			//         item->widget()->parent()->objectName();

			//         item->widget()->setVisible(false);
			//         delete item;
			//     }
			//     delete this->layout();
			// }

			foreach (QObject* child, this->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			QLayout* postojeciLayout = this->layout();
			if (postojeciLayout)
			{
				delete postojeciLayout;
			}
			prikaziEdukativniTekst();
		});
	}
}

void Skriveni::btnNasaoVucica_clicked()
{

	if (nasaoVucica)
	{

		if (!tbOpis->isVisible())
		{
			tbOpis->setVisible(true);
		}
		tbOpis->setText("'Vucicu, odlazi!' je simbol sa poslednje odrzanih protesta, Srbija "
						"protiv Nasilja, kada nam je svima samo prekipilo.........");
	}
}

void Skriveni::btnNasaoMetu_clicked()
{

	if (nasaoMetu)
	{

		if (!tbOpis->isVisible())
		{
			tbOpis->setVisible(true);
		}
		tbOpis->setText("Tog bombardovanja, kada je prikazan onaj pravi srpski karakter, kada "
						"su ljudi izlazili na ulice noseci mete i pevali Klintonu..");
	}
}

void Skriveni::btnNasaoZastavu_clicked()
{

	if (nasaoZastavu)
	{
		if (!tbOpis->isVisible())
		{
			tbOpis->setVisible(true);
		}
		tbOpis->setText("A protesta 1996, zabranili su zastave koje simbolizuju bilo kakva "
						"politicka obelezja. Ferari zastava nije politicko obelezje, zar ne?");
	}
}

void Skriveni::btnNasaoTransparent_clicked()
{

	if (nasaoTransparent)
	{
		if (!tbOpis->isVisible())
		{
			tbOpis->setVisible(true);
		}
		tbOpis->setText("Samo jedan transparent sa protesta Srbija protiv nasilja...");
	}
}

void Skriveni::btnNasaoSuzavac_clicked()
{
	if (nasaoSuzavac)
	{
		if (!tbOpis->isVisible())
		{
			tbOpis->setVisible(true);
		}
		tbOpis->setText("Protesti 2016. Da li se secate kako je policija suzavcima "
						"i konjima rasterivala gradjane?");
	}
}

void Skriveni::btnNasaoMegafon_clicked()
{
	if (nasaoMegafon)
	{
		if (!tbOpis->isVisible())
		{
			tbOpis->setVisible(true);
		}
		tbOpis->setText("Kako protesti da prodju bez megafona? STUDENTI NE CUTE!");
	}
}
