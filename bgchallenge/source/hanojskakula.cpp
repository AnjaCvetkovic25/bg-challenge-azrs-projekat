#include "../include/hanojskakula.h"
#include <QDebug>
#include <QGraphicsProxyWidget>
#include <QGraphicsSceneDragDropEvent>
#include <QGridLayout>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>

Hanojskakula::Hanojskakula(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(1, 1, vreme, pocetnoVreme, parent), ui(new Ui::Hanojskakula)
{
	ui->setupUi(this);

	ui->stub1->setParent(nullptr);
	ui->stub2->setParent(nullptr);
	ui->stub3->setParent(nullptr);

	setImagePath(":/new/pozadine/resources/geneksBg.jpg");

	// inicijalizacija QGraphicsView i QGraphicsScene
	viewHanojskeKule = ui->viewHanojskaKula;
	sceneHanojskeKule = new QGraphicsScene(this);
	viewHanojskeKule->setScene(sceneHanojskeKule);
	sceneHanojskeKule->setSceneRect(0, 0, 400, 227);

	Tekstovi();
	connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		ui->lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	QPixmap stuboviSlika(":/resources/hanojska_kula/stub.png");
	ui->stub1->setPixmap(stuboviSlika);
	ui->stub2->setPixmap(stuboviSlika);
	ui->stub3->setPixmap(stuboviSlika);

	// dodavanje stubica
	proxyStub1 = new QGraphicsProxyWidget();
	proxyStub2 = new QGraphicsProxyWidget();
	proxyStub3 = new QGraphicsProxyWidget();
	proxyStub1->setWidget(ui->stub1);
	proxyStub2->setWidget(ui->stub2);
	proxyStub3->setWidget(ui->stub3);
	proxyStub1->setAcceptDrops(true);
	proxyStub2->setAcceptDrops(true);
	proxyStub3->setAcceptDrops(true);
	sceneHanojskeKule->addItem(proxyStub1);
	sceneHanojskeKule->addItem(proxyStub2);
	sceneHanojskeKule->addItem(proxyStub3);
	// postavi stubice na scenu
	proxyStub1->setPos(-100, 0);
	proxyStub2->setPos(100, 0);
	proxyStub3->setPos(300, 0);

	// ucitavanje diskova
	QPixmap diskoviSlika(":/resources/hanojska_kula/diskovi.png");
	if (diskoviSlika.isNull())
	{
		qDebug() << "Slika nije uspešno učitana!";
		return;
	}

	// izdvajanje delova slike za svaki disk
	QPixmap disk1Slika = diskoviSlika.copy(77, 0, 155, 51);
	QPixmap disk2Slika = diskoviSlika.copy(35, 52, 235, 51);
	QPixmap disk3Slika = diskoviSlika.copy(0, 52 + 51, 320, 51);
	disk1Item = new QGraphicsPixmapItem(disk1Slika);
	disk2Item = new QGraphicsPixmapItem(disk2Slika);
	disk3Item = new QGraphicsPixmapItem(disk3Slika);

	postaviPocetnoStanje();

	disk1Item->setScale(0.4);
	disk2Item->setScale(0.4);
	disk3Item->setScale(0.4);
	sceneHanojskeKule->addItem(disk1Item);
	sceneHanojskeKule->addItem(disk2Item);
	sceneHanojskeKule->addItem(disk3Item);

	viewHanojskeKule->setDragMode(QGraphicsView::ScrollHandDrag);
	sceneHanojskeKule->installEventFilter(this);
	viewHanojskeKule->show();

	connect(ui->retry, &QPushButton::clicked, this, &Hanojskakula::on_RetryClicked);
	connect(ui->pbNazad, &QPushButton::clicked, this, &Hanojskakula::zatvori);
	connect(ui->pbPomoc, &QPushButton::clicked, this, &Hanojskakula::pomoc);
}

void Hanojskakula::pomoc()
{
	QDialog dijalog;
	dijalog.setFixedHeight(300);
	dijalog.setFixedWidth(400);
	dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
						  "rgb(200,200,200); margin: 10px;}");

	QLabel* text = new QLabel("Kao što je Geneks kula simbol socijalističkog Beograda, tako su "
							  "hanojske kule jedan simboličan programerski zadatak. Da li mozete da "
							  "prebacite sve diskove na treću kulu, od najvećeg do najmanjeg? Srećno!",
							  &dijalog);
	text->setAlignment(Qt::AlignCenter);
	text->setWordWrap(true);
	text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
	QPushButton* ok = new QPushButton("OK", &dijalog);
	ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
					  "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
					  "padding: 10px; border-radius: 21px; font: bold 14px;}"
					  "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
					  "#bcbcbc; }"
					  "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
					  "#a0a0a0; }");
	QVBoxLayout* layout = new QVBoxLayout(&dijalog);

	layout->addWidget(text);

	layout->addWidget(ok);
	connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
	dijalog.exec();
	pomocExecuted = true;
}

bool Hanojskakula::eventFilter(QObject* watched, QEvent* event)
{
	if (watched == sceneHanojskeKule)
	{
		if (event->type() == QEvent::GraphicsSceneMousePress)
		{
			QGraphicsSceneMouseEvent* mouseEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
			QGraphicsItem* item = sceneHanojskeKule->itemAt(mouseEvent->scenePos(), viewHanojskeKule->transform());
			// Ako je kliknuto na disk
			if (item && item->type() == QGraphicsPixmapItem::Type && (item->flags() & QGraphicsItem::ItemIsMovable))
			{
				izabranDisk = dynamic_cast<QGraphicsPixmapItem*>(item);
				inicijalnaPozicijaDiska = izabranDisk->pos();
				sceneHanojskeKule->setFocusItem(izabranDisk);

				QList<QGraphicsItem*> itemNaPoz = sceneHanojskeKule->items(item->pos());
				// pronadji prvi QGraphicsProxyWidget u listi
				for (QGraphicsItem* item : itemNaPoz)
				{
					if (item->type() == QGraphicsProxyWidget::Type)
					{
						proxyInicijalniStub = dynamic_cast<QGraphicsProxyWidget*>(item);
						break;
					}
				}
				// qDebug() << "Proxy stub s" << proxyInicijalniStub;
				sceneHanojskeKule->setFocusItem(izabranDisk);

				QStack<unsigned char>& stek = vratiOdgovarajuciStek(proxyInicijalniStub);
				smanjiStek(stek);
			}
		}
		else if (event->type() == QEvent::GraphicsSceneMouseRelease && izabranDisk)
		{
			QGraphicsSceneMouseEvent* mouseEvent = static_cast<QGraphicsSceneMouseEvent*>(event);
			QGraphicsItem* dropItem = sceneHanojskeKule->itemAt(mouseEvent->scenePos(), viewHanojskeKule->transform());

			QList<QGraphicsItem*> itemNaPoz = sceneHanojskeKule->items(dropItem->pos());
			QGraphicsProxyWidget* proxyCiljniStub = nullptr;
			// pronadji prvi QGraphicsProxyWidget u listi
			for (QGraphicsItem* item : itemNaPoz)
			{
				if (item->type() == QGraphicsProxyWidget::Type)
				{
					proxyCiljniStub = dynamic_cast<QGraphicsProxyWidget*>(item);
					break;
				}
			}
			sceneHanojskeKule->setFocusItem(izabranDisk);
			if (proxyCiljniStub && proxyInicijalniStub)
			{
				obradiDropDiska(*proxyInicijalniStub, *proxyCiljniStub, inicijalnaPozicijaDiska);
			}
			// resetujemo povuceni disk i inicijalni stub
			izabranDisk = nullptr;
			proxyInicijalniStub = nullptr;
			sceneHanojskeKule->setFocusItem(nullptr);
		}
	}
	return QWidget::eventFilter(watched, event);
}

void Hanojskakula::obradiDropDiska(QGraphicsProxyWidget& proxyIzvorniStub, QGraphicsProxyWidget& proxyCiljniStub,
								   QPointF inicijalnaPozicijaDiska)
{

	QLabel* ciljniStub = dynamic_cast<QLabel*>(proxyCiljniStub.widget());
	QStack<unsigned char>& ciljniStek = vratiOdgovarajuciStek(&proxyCiljniStub);
	QStack<unsigned char>& izvorniStek = vratiOdgovarajuciStek(&proxyIzvorniStub);

	// ako je ciljni stub prazan
	if (ciljniStek.empty())
	{
		postaviDiskNaStub(izabranDisk, ciljniStek, ciljniStub, 0);
	}
	else if (dozvoljenPotez(izabranDisk, ciljniStek))
	{
		if (ciljniStek.size() == 1 || ciljniStek.size() == 2)
		{
			promeniDozvolu(ciljniStek, false);
			int y = (ciljniStek.size() == 1) ? -20 : -40;
			postaviDiskNaStub(izabranDisk, ciljniStek, ciljniStub, y);

			if (stub3.size() == 3)
			{
				prikaziPorukuPobede();
			}
		}
	}
	else
	{
		promeniDozvolu(izvorniStek, false);
		if (izabranDisk == disk2Item)
		{ // zeleni
			izvorniStek.push(2);
		}
		else
		{ // crveni;
			izvorniStek.push(3);
		}
		izabranDisk->setPos(inicijalnaPozicijaDiska);
	}
}

void Hanojskakula::postaviDiskNaStub(QGraphicsPixmapItem* disk, QStack<unsigned char>& stek, QLabel* ciljniStub,
									 int yOffset)
{
	int x = 0;
	int y = 0;

	// postavljanje odgovarajucih vrednosti za x i y u zavisnosti od diska
	if (disk == disk1Item)
	{ // zuti
		x = 70;
		y = 178 + yOffset;
		stek.push(1);
	}
	else if (disk == disk2Item)
	{ // zeleni
		x = 54;
		y = 179 + yOffset;
		stek.push(2);
	}
	else
	{ // crveni
		x = 40;
		y = 179 + yOffset;
		stek.push(3);
	}

	qreal diskX = ciljniStub->pos().x() + x;
	qreal diskY = ciljniStub->pos().y() + y;
	disk->setPos(diskX, diskY);
}

void Hanojskakula::postaviPocetnoStanje()
{
	if (disk1Item && disk2Item && disk3Item)
	{
		disk3Item->setPos(-100 + 40, 179);		// crveni
		disk2Item->setPos(-100 + 54, 179 - 20); // zeleni
		disk1Item->setPos(-100 + 70, 179 - 40); // zuti
		disk1Item->setFlag(QGraphicsItem::ItemIsMovable);
		disk2Item->setFlag(QGraphicsItem::ItemIsMovable, false);
		disk3Item->setFlag(QGraphicsItem::ItemIsMovable, false);
		stub1.clear();
		stub2.clear();
		stub3.clear();
		stub1.push(3);
		stub1.push(2);
		stub1.push(1);
	}
}

QStack<unsigned char>& Hanojskakula::vratiOdgovarajuciStek(QGraphicsProxyWidget* proxyCiljniStub)
{

	if (proxyCiljniStub == proxyStub1)
	{
		return stub1;
	}
	else if (proxyCiljniStub == proxyStub2)
	{
		return stub2;
	}
	return stub3;
}
bool Hanojskakula::dozvoljenPotez(QGraphicsPixmapItem* izabranDisk, QStack<unsigned char> stub)
{
	unsigned char velicinaDraggedDiska = 0;
	if (izabranDisk == disk1Item)
	{ // zuti
		velicinaDraggedDiska = 1;
	}
	else if (izabranDisk == disk2Item)
	{ // zeleni
		velicinaDraggedDiska = 2;
	}
	else
	{ // crveni
		velicinaDraggedDiska = 3;
	}

	// da bi potez bio dozvoljen mora da disk na vrhu bude veci od velicine diska
	// koji se spusta
	return stub.top() > velicinaDraggedDiska ? true : false;
}

void Hanojskakula::smanjiStek(QStack<unsigned char>& stub)
{
	if (!stub.isEmpty())
	{
		stub.pop();
	}
	promeniDozvolu(stub, true);
}

void Hanojskakula::promeniDozvolu(QStack<unsigned char> stub, bool dozvola)
{
	if (!stub.isEmpty())
	{
		if (stub.top() == 2)
		{
			disk2Item->setFlag(QGraphicsItem::ItemIsMovable, dozvola);
		}
		else
		{
			disk3Item->setFlag(QGraphicsItem::ItemIsMovable, dozvola);
		}
	}
}

void Hanojskakula::prikaziPorukuPobede()
{
	QMessageBox msgBox;
	msgBox.setWindowTitle(" ");
	msgBox.setText("Bravo!");

	QAbstractButton* igrajOpetButton = msgBox.addButton("Igraj opet", QMessageBox::ActionRole);
	QAbstractButton* daljeButton = msgBox.addButton("Dalje", QMessageBox::ActionRole);

	msgBox.setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);
	igrajOpetButton->setStyleSheet(buttonStylesheet);
	daljeButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();

	if (msgBox.clickedButton() == igrajOpetButton)
	{
		postaviPocetnoStanje();
	}
	else if (msgBox.clickedButton() == daljeButton)
	{

		if (this->layout() != nullptr)
		{
			QLayoutItem* item;
			while ((item = this->layout()->takeAt(0)) != nullptr)
			{
				qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

				delete item->widget();
				delete item;
			}
			delete this->layout();
		}
		QObjectList children = this->children();
		for (QObject* child : children)
		{
			if (QWidget* widget = qobject_cast<QWidget*>(child))
			{
				widget->setVisible(false);
			}
		}
		QLayout* postojeciLayout = this->layout();
		if (postojeciLayout)
		{
			delete postojeciLayout;
		}
		this->prikaziEdukativniTekst();
	}
}

void Hanojskakula::Tekstovi()
{

	m_edukativniTekst = R"(
Beograd ima dve urbanističke kapije. Jedna je neuspela. To je Istočna – zvana Rudo. Čine je tri stepenasta solitera, međusobno okrenuta leđima, kao da ne govore.
Nasuprot Istočnoj, Zapadna kapija je do te mere uspelo delo da ju je njujorški muzej MoMa uvrstio u izložbu jugoslovenske arhitekture!

U narodu je Zapadna kapija poznatija pod imenom Geneksove kule. U kuli bližoj auto-putu najveća spoljnotrgovinska firma Jugoslacije Generaleksport, skraćeno Geneks, našla je sebi odgovarajuće sedište. Poslovanje po rubnim kontinentima i milijarde dolara prometa tražili su upravo takvu poslovnu zgradu: moderno monumentalnu. Prvi đakuzi koji je uvezen u Jugoslaviju ugrađen je u kabinu generalnog direktora.

Geneks je izrastao u državu u državi. Osnovali su ga 1952. najsposobniji islednici Uprave državne bezbednosti sa zadatkom da se razbije trgovinska blokada Sovjetskog saveza i razviju ekonomske veze sa kapitalističkim svetom. Šezdest predstavništava u svetu trgovalo je sa obe strane gvozdene zavese. Prihvatali su se I poslovi u kriznim područjima. U Africi i na Bliskom istoku je važilo pravilo: Ako si rešio da počneš rat, prvo pozovi Geneks!

Velika firma je bučno propala u opštem raspadu Jugoslavije. U njenim prostorijama sada rade neke daleko manje kompanije koje nikada neće obnoviti sjaj Titove doktrine trećeg puta u međunarodnoj trgovini.
Za stanare Geneksa, veliki trag u srcu ostavljaju polukružne narandžaste stepenice, gde se klinci igraju žmurke I fudbala, a tinejdžeri piju pivo. Tu se stvaraju prve uspomene, prve ljubavi i ostale čari detinjstva.)";

	m_asocijacija = "Prva stanica tramvaja 7 nakon nakon Ekonomskog fakulteta";

	m_hint = "Iskoristi Moovit, slobodno! Obrati pažnju na smer!";

	m_resenjeAsocijacije.append("staro sajmiste");
	m_resenjeAsocijacije.append("staro sajmište");

	m_naslovEdukativnogTeksta = "O Geneks kuli";
}

Hanojskakula::~Hanojskakula()
{
	delete ui;
}

void Hanojskakula::zapocniChallenge()
{
}
void Hanojskakula::zavrsiChallenge()
{
}

void Hanojskakula::on_RetryClicked()
{
	postaviPocetnoStanje();
}
