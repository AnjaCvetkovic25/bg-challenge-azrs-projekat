
#include "../include/igrac.h"
#include <QDebug>
#include <QFile>
#include <QTextStream>

Igrac::Igrac()
{
	// Implementacija konstruktora ako je potrebno
}

Igrac::Igrac(const QString& ime) : m_ime(ime), m_vreme(0), m_poslednjiChallenge(0), m_nivo(1)
{
}

Igrac::Igrac(const QString& ime, int vreme, int nivo, int izazov)
	: m_ime(ime), m_vreme(vreme), m_nivo(nivo), m_poslednjiChallenge(izazov)
{
}

QString Igrac::getIme() const
{
	return m_ime;
}

int Igrac::getVreme() const
{
	return m_vreme;
}

int Igrac::getNivo() const
{
	return m_nivo;
}

int Igrac::getPoslednjiOtkljucaniChallenge() const
{
	return m_poslednjiChallenge;
}

void Igrac::setPoslednjiOtkljucaniChallenge(int redniBr)
{
	m_poslednjiChallenge = redniBr;
}

void Igrac::setNivo(int nivo)
{
	m_nivo = nivo;
}

void Igrac::setVreme(int vreme)
{
	m_vreme += (vreme - m_vreme);
}

void Igrac::sacuvaj()
{
	// stavila sam ovaj path jer gleda u odnosu na working directory, a kad
	// prosledim putanju iz .qrc fajla onda ne moze da ga otvori ne nekog razloga
	QFile file("../bgchallenge/resources/savefile.txt");
	if (!file.exists())
		qDebug() << "file does not exist";

	QString data = m_ime + " " + QString::number(m_vreme) + " " + QString::number(m_nivo) + " " +
				   QString::number(m_poslednjiChallenge);
	qDebug() << data;

	if (file.open(QIODevice::Append | QIODevice::Text))
	{
		QTextStream stream(&file);
		stream << data << "\n";

		file.flush();
		file.close();
		qDebug() << "Data successfully written to file.";
	}
	else
	{
		qDebug() << "Failed to open file for writing." << file.errorString();
	}
}

QJsonObject Igrac::toJson() const
{
	QJsonObject json;
	json["ime"] = m_ime;
	json["vreme"] = m_vreme;
	json["nivo"] = m_nivo;
	json["izazov"] = m_poslednjiChallenge;
	return json;
}

Igrac Igrac::fromJson(const QJsonObject& json)
{
	QString ime = json["ime"].toString();
	int vreme = json["vreme"].toInt();
	int nivo = json["nivo"].toInt();
	int izazov = json["izazov"].toInt();
	return Igrac(ime, vreme, nivo, izazov);
}
