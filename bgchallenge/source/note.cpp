#include "../include/note.h"

#include <QDebug>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPixmap>
#include <QRandomGenerator>
#include <QSpacerItem>
#include <QUiLoader>

#include <QDialog>

Note::Note(Timer* timer, int pocetnoVreme, QWidget* parent) : Challenge_test(12, 3, timer, pocetnoVreme, parent)
{
	// ui->setupUi(this);
	this->setWindowTitle("BgChallenge");
	this->setMinimumWidth(1280);
	this->setMinimumHeight(720);
	this->setFixedSize(this->width(), this->height());

	this->setImagePath(":/new/pozadine/resources/tasmajdan1(1).jpg");
	this->setWindowIcon(QIcon(":/new/resources/forms/note.ui"));

	QFile file(":/new/resources/forms/note.ui");

	if (!file.open(QFile::ReadOnly))
	{
		qDebug() << "Error opening UI file:" << file.errorString();
		return;
	}

	QUiLoader loader;
	ui = loader.load(&file, this);
	ui->setFixedSize(this->width(), this->height());

	file.close();

	QPushButton* nazadBtn = ui->findChild<QPushButton*>("pbNazad");
	connect(nazadBtn, &QPushButton::clicked, this, &Note::zatvori);
	QLCDNumber* lcdNumber = ui->findChild<QLCDNumber*>("lcdNumber");

	connect(ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &Note::pomoc);

	connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	Tekstovi();

	ispravnaKombinacija = "doDOsiDOsisolfamiremidoDO";
	kombinacija = "";
	noteIspravnaKombinacija = QList<QPushButton*>();
	brojac = new int(-1);

	nota = new QMediaPlayer();
	notaAO = new QAudioOutput();

	QPushButton* do1 = ui->findChild<QPushButton*>("do_1");
	QPushButton* do22 = ui->findChild<QPushButton*>("do2_2");
	QPushButton* si3 = ui->findChild<QPushButton*>("si_3");
	QPushButton* do24 = ui->findChild<QPushButton*>("do2_4");
	QPushButton* si5 = ui->findChild<QPushButton*>("si_5");
	QPushButton* sol6 = ui->findChild<QPushButton*>("sol_6");
	QPushButton* fa7 = ui->findChild<QPushButton*>("fa_7");
	QPushButton* mi11 = ui->findChild<QPushButton*>("mi_11");
	QPushButton* re12 = ui->findChild<QPushButton*>("re_12");
	QPushButton* mi13 = ui->findChild<QPushButton*>("mi_13");
	QPushButton* do14 = ui->findChild<QPushButton*>("do_14");
	QPushButton* do215 = ui->findChild<QPushButton*>("do2_15");

	noteIspravnaKombinacija.append(do1);
	noteIspravnaKombinacija.append(do22);
	noteIspravnaKombinacija.append(si3);
	noteIspravnaKombinacija.append(do24);
	noteIspravnaKombinacija.append(si5);
	noteIspravnaKombinacija.append(sol6);
	noteIspravnaKombinacija.append(fa7);
	noteIspravnaKombinacija.append(mi11);
	noteIspravnaKombinacija.append(re12);
	noteIspravnaKombinacija.append(mi13);
	noteIspravnaKombinacija.append(do14);
	noteIspravnaKombinacija.append(do215);

	btnDo = ui->findChild<QPushButton*>("btnDo");
	btnRe = ui->findChild<QPushButton*>("btnRe");
	btnMi = ui->findChild<QPushButton*>("btnMi");
	btnFa = ui->findChild<QPushButton*>("btnFa");
	btnSol = ui->findChild<QPushButton*>("btnSol");
	btnLa = ui->findChild<QPushButton*>("btnLa");
	btnSi = ui->findChild<QPushButton*>("btnSi");
	btnDo2 = ui->findChild<QPushButton*>("btnDo2");

	connect(btnDo, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnDo, &kombinacija, "qrc:/new/note/resources/note/316899__jaz_the_man_2__do-stretched.wav");
	});
	connect(btnRe, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnRe, &kombinacija, "qrc:/new/note/resources/note/316909__jaz_the_man_2__re-stretched.wav");
	});
	connect(btnMi, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnMi, &kombinacija, "qrc:/new/note/resources/note/316907__jaz_the_man_2__mi-stretched.wav");
	});
	connect(btnFa, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnFa, &kombinacija, "qrc:/new/note/resources/note/316905__jaz_the_man_2__fa-stretched.wav");
	});
	connect(btnSol, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnSol, &kombinacija,
					"qrc:/new/note/resources/note/"
					"316911__jaz_the_man_2__sol-stretched.wav");
	});
	connect(btnLa, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnLa, &kombinacija, "qrc:/new/note/resources/note/316910__jaz_the_man_2__si-stretched.wav");
	});
	connect(btnSi, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnSi, &kombinacija, "qrc:/new/note/resources/note/316903__jaz_the_man_2__la-stretched.wav");
	});
	connect(btnDo2, &QPushButton::clicked, this, [=]() {
		kliknuoNotu(btnDo2, &kombinacija,
					"qrc:/new/note/resources/note/"
					"316900__jaz_the_man_2__do-stretched-octave.wav");
	});
}

Note::~Note()
{
	if (nota != nullptr)
	{
		qDebug() << "obrisan player";
		delete nota;
		nota = nullptr;
	};

	if (notaAO != nullptr)
	{
		qDebug() << "obrisan audioOutput";
		delete notaAO;
		notaAO = nullptr;
	};

	delete brojac;

	delete ui;
}

void Note::Tekstovi()
{
	m_edukativniTekst = R"(
Tašmajdan park je svedok burne srpske istorije. O tome kako je nastao, razvijao i korišćen u razlčitim vekovima ostale su brojne legende, a pravu misteriju čine glasine o podzemnoj pećini ispod Tašmajdana, koja je ostala neistražena.

Poznato je da je Tašmajdan park služio kao turski kamenolom odakle potiče i sam naziv parka. Tašmajdan je složenica turskih reči “tas” i “meyden” što se prevodi kao mesto odakle se vadi kamen ili rudnik. Mnogi istorijski izvori potvrđuju da je reč o kamenolomu odakle je vađen kamen za zidanje starih beogradskih zdanja. Vađenjem kamena nastale su brojne katakombe koje su kasnije kroz istoriju služile kao vojni magacini, skladišta i skloništa za ranjene vojnike.

Priče o kamenolomu sežu još iz doba Rimljana i veruje se da je kamen korišćen za izgradnju vodovoda i hramova u Singidunumu, a pronađeni su nadgrobni spomenici napravljeni upravo od kamena iz Tašmajdan parka. Kasnije su Osmanlije sa ovog mesta vadile šalitru (kalijum nitrat) za izradu baruta.

Odluka o uređenju parka doneta je nakon Drugog svetskog rata kada je planirano da se na gornjem platou uredi park, a sadašnji stadion na prostoru gde se nekada nalazio kamenolom. U kasnijim godinama napravljen je sportsko-rekreativni centar sa otvorenim i zatvorenim bazenom. Svi radovi na Tašmajdanu izvođeni su ručno i na izgradnji je bilo zaposleno gotovo 900 ljudi. Sadnice su kupljene i prenošene zaprežnim kolima iz rasadnika u Krnjači i Zagrebu.

Tašmajdanske pećine, koje su nedavno otvorene za posetioce kriju istoriju Beograda od pre dve hiljade godina do najsavremenijih dana, svedoče o vremenu Drugog svetskog rata kada su korišćene kao skloništa i kasnije kao jedno od prvih modernih podzemnih skloništa u Beogradu. Danas to predstavlja spomenik i svedok je jednog prošlog vremena koji predstavlja raskošan prostor ovog grada.
Spomenik našoj pesnikinji Desanki Maksimović podignut je 2007. godine na Tašmajdanu odmah ispred dečijeg igrališta. Desanka Maksimović uživala je u šetnji kroz Tašmajdan zbog čega joj je i posvećen spomenik";)";
	m_hint = "I tebe sam sit kafano";
	m_asocijacija = "Boemska ulica";
	m_resenjeAsocijacije.append("skadarlija");
	m_resenjeAsocijacije.append("skadarska");
	m_naslovEdukativnogTeksta.append("O Tašmajdanu");
}

void Note::reprodukcijaZvuka(QMediaPlayer* player)
{
	player->play();
}
void Note::pustiNotu(QString url)
{
	/*    nota = new QMediaPlayer();
		notaAO = new QAudioOutput()*/
	;
	nota->setAudioOutput(notaAO);
	nota->setSource(QUrl(url));
	notaAO->setVolume(10);
	nota->play();
}

void Note::kliknuoNotu(QPushButton* clickedButton, QString* kombinacija, QString url)
{

	if (clickedButton)
	{

		(*kombinacija).append(clickedButton->text());

		if (ispravnaKombinacija.startsWith(*kombinacija))
		{

			(*brojac)++;
			noteIspravnaKombinacija[*brojac]->setStyleSheet(
				"border-image: url(:/new/note/resources/note/tacnanota.png); color: "
				"rgba(0,0,0,0);");

			this->pustiNotu(url);

			qDebug() << *kombinacija;

			if (*kombinacija == ispravnaKombinacija)
			{
				qDebug() << "pogodjena je kombinacija!!!";
				QTimer::singleShot(1000, this, [=]() {
					foreach (QObject* child, this->children())
					{
						if (child->isWidgetType())
						{
							QWidget* childWidget = qobject_cast<QWidget*>(child);
							if (childWidget)
							{
								childWidget->hide();
							}
						}
					}
					prikaziEdukativniTekst();
				});
			}
		}
		else
		{

			(*brojac)++;
			noteIspravnaKombinacija[*brojac]->setStyleSheet(
				"border-image: url(:/new/note/resources/note/pogresnanota.png); "
				"color: rgba(0,0,0,0);");
			this->pustiNotu(url);
			(*kombinacija) = "";

			(*brojac) = -1;
			qDebug() << "pogresio si!!!";

			obrni(btnDo, btnRe, btnMi, btnFa, btnSol, btnLa, btnSi, btnDo2);

			QTimer::singleShot(1100, this, [=]() {
				obrni(btnDo, btnRe, btnMi, btnFa, btnSol, btnLa, btnSi, btnDo2);

				for (QPushButton* ispravnaNota : noteIspravnaKombinacija)
				{
					ispravnaNota->setStyleSheet("border-image: url(:/new/note/resources/note/nota.png); color: "
												"rgba(0,0,0,0);");
				}
			});
		}
	}
	else
	{
		qDebug() << "nema sendera";
	}
}

void Note::obrni(QPushButton* btnDo, QPushButton* btnRe, QPushButton* btnMi, QPushButton* btnFa, QPushButton* btnSol,
				 QPushButton* btnLa, QPushButton* btnSi, QPushButton* btnDo2)
{

	btnDo->setEnabled(!btnDo->isEnabled());
	btnRe->setEnabled(!btnRe->isEnabled());
	btnMi->setEnabled(!btnMi->isEnabled());
	btnFa->setEnabled(!btnFa->isEnabled());
	btnSol->setEnabled(!btnSol->isEnabled());
	btnLa->setEnabled(!btnLa->isEnabled());
	btnSi->setEnabled(!btnSi->isEnabled());
	btnDo2->setEnabled(!btnDo2->isEnabled());
}

void Note::pomoc()
{
	QDialog dijalog;
	dijalog.setFixedHeight(300);
	dijalog.setFixedWidth(400);
	dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
						  "rgb(200,200,200); margin: 10px;}");

	QLabel* text = new QLabel("Zadatak ovde je ponoviti muzičku sekvencu. Prva nota u sekvenci je nota "
							  "do - prva tipka. Sećaš li se muzičkog iz osnovne?",
							  &dijalog);
	text->setAlignment(Qt::AlignCenter);
	text->setWordWrap(true);
	text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
	QPushButton* ok = new QPushButton("OK", &dijalog);
	ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
					  "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
					  "padding: 10px; border-radius: 21px; font: bold 14px;}"
					  "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
					  "#bcbcbc; }"
					  "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
					  "#a0a0a0; }");
	QVBoxLayout* layout = new QVBoxLayout(&dijalog);

	layout->addWidget(text);

	layout->addWidget(ok);
	connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
	dijalog.exec();
}
