#include "../include/puzla.h"
#include "../forms/ui_puzla.h"

#include <QMessageBox>

Puzla::Puzla(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(4, 1, vreme, pocetnoVreme, parent), ui(new Ui::Puzla)
{
	ui->setupUi(this);

	// TODO: STAVITI POZADINU
	setImagePath(":/resources/gardosnoc.jpg");

	Tekstovi();

	connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		ui->lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});
	connect(ui->pbNazad, &QPushButton::clicked, this, &Puzla::zatvori);
	ui->lSifra->hide();
	ui->leSifra->hide();
	ui->label->hide();

	connect(ui->pbSlika1, &QPushButton::clicked, this, &Puzla::onPbSlika1Clicked);
	connect(ui->pbSlika2, &QPushButton::clicked, this, &Puzla::onPbSlika2Cilcked);
	connect(ui->pbSlika3, &QPushButton::clicked, this, &Puzla::onPbSlika3Clicked);
	connect(ui->pbSlika4, &QPushButton::clicked, this, &Puzla::obPbSlika4Clicked);
	connect(ui->pbSlika5, &QPushButton::clicked, this, &Puzla::onPbSlika5Clicked);
	connect(ui->pbSlika6, &QPushButton::clicked, this, &Puzla::onPbSlika6Clicked);
	connect(ui->pbMissingPiece, &QPushButton::clicked, this, &Puzla::onPbMissingPieceClicked);
	connect(ui->pbV1, &QPushButton::clicked, this, &Puzla::onPbV1Clicked);
}

Puzla::~Puzla()
{
	delete ui;
}

void Puzla::onPbSlika1Clicked()
{
	QMessageBox::information(this, "Puzzle", "Pokusajte ponovo!");
	ui->pbSlika1->setEnabled(false);
}

void Puzla::onPbSlika2Cilcked()
{
	QMessageBox::information(this, "Puzzle", "Pokusajte ponovo");
	ui->pbSlika2->setEnabled(false);
}

void Puzla::onPbSlika3Clicked()
{
	QMessageBox::information(this, "Puzzle", "Pokusaj ponovo!");
	ui->pbSlika3->setEnabled(false);
}

void Puzla::obPbSlika4Clicked()
{
	QMessageBox::information(this, "Puzzle", "Pokusaj ponovo!");
	ui->pbSlika4->setEnabled(false);
}

void Puzla::onPbSlika5Clicked()
{
	QMessageBox::information(this, "Puzzle", "Pokusajte ponovo!");
	ui->pbSlika5->setEnabled(false);
}

void Puzla::onPbSlika6Clicked()
{
	QMessageBox::information(this, "Puzzle", "Pokusajte ponovo!");
	ui->pbSlika6->setEnabled(false);
}

void Puzla::onPbV1Clicked()
{
	QMessageBox::information(this, "Puzzle", "Pokusajet ponovo!");
	ui->pbV1->setEnabled(false);
}

void Puzla::onPbMissingPieceClicked()
{
	ui->frame->hide();
	ui->lSifra->show();
	ui->leSifra->show();
	ui->label->show();

	QObjectList children = this->children();
	for (QObject* child : children)
	{
		// qDebug() << child->objectName();
		if (QWidget* widget = qobject_cast<QWidget*>(child))
		{
			// qDebug() << widget->objectName() << widget->parent()->objectName();
			widget->setVisible(false);
		}
	}
	QLayout* postojeciLayout = this->layout();
	if (postojeciLayout)
	{
		delete postojeciLayout;
	}

	prikaziEdukativniTekst();
}

void Puzla::Tekstovi()
{
	m_edukativniTekst = R"(
Kulu su izgradile mađarske vlasti 1896. godine u čast proslave hiljadugodišnje vladavine na ovim prostorima. Odatle i pravo ime ove građevine – Milenijumska kula.
Prema legendi, Mađarska je nastala spajanjem sedam plemena pa je zbog toga bilo simbolički važno podići i sedam spomenika. Dva posvećena ovoj godišnjici izgrađena su u Mađarskoj,dva u današnjoj Slovačkoj, jedan u Ukrajini, jedan u Rumuniji i ovaj sedmi - Zemunska kula koji se nalazio najjužnije. Centralni Milenijumski spomenik podignut je u Budimpešti i nalazi se na Trgu heroja.

Decenijama su o ovom mestu ispredane misteriozne priče. Jedna od takvih legendi je da iz podruma Milenijumske kule vodi tunel kojim se, ispod Dunava, može stići do tvrđave na Kalemegdanu.
Ipak, jedan drugi mit zaslužan je za ime koje se mnogo češće pominje u pričama Zemunaca. Naime, pre nego Milenijumska ili čak i Gardoš kula, Zemunci će vam reći da se ona zove kula Sibinjanin Janka.

Sibinjanin Janko je zapravo Janoš Hunjadi, mađarski vitez koji je 1456. godine pobedio Tursko carstvo, odbranio Beograd i ostvario najveća pobedu hrišćana nad Osmanlijama u 15. veku.
Samo tri nedelje po okončanju bitke, dok je slavlje zbog pobede još trajalo, Hunjadi je umro od posledica kuge, priča kaže, baš u jednoj od kula srednjovekovne tvrđave na Gardošu.
Sve ovo dešavalo se 440 godina pre zidanja Milenijumske kule sa kojom Sibinjanin Janko nije imao nikakve veze, ali je njegov kult u narodu i dalje bio jak, pa nije prošlo mnogo pre nego što su mađarsku kulu Srbi prozvali – kula Sibinjanina Janka.

Jedan zid u Zemunu povezao je i naciste i partizane, i Srbe i Nemce, Hrvate i Mađare, Jevreje… To je jedan zid običnih ljudi koje je sudbina u nekom trenutku istorije dovela na najlepše mesto u Zemunu – Kulu na Gardošu.
Malo koje mesto tako upečatljivo daje direktan uvid u živote i sudbine “običnih ljudi” u svim turbulencijama ali i mirnim periodima 20. veka.
Da li se jedan Konig ikad vratio u rodni Rajhenberg, odakle je sigurno kao vojnik, stigao u Zemun. Kako li je izgledao trenutak mira jednog naciste poslatog na front, koji se u okupiranom gradu obreo na vrh kule i ostavio svoje ime?
Da li su još među nama Baćko i Sonja koji su svoja imena ovekovečili 1956. godine?
Koliko li potomaka ima Imre, koji je svoje ime urezao 1910. godine?
Mali, običan čovek, vojnik – oslobodilac ili okupator, Srbin ili Nemac, Jevrejnim ili Hrvat, svi su želeli samo da ostave svoje ime, godinu kada su bili na tom magičnom mestu i grad odakle su došli. Ili Ime drage koju su voleli.)";
	m_asocijacija = "Prokop";
	m_resenjeAsocijacije.append("železnička stanica");
	m_resenjeAsocijacije.append("glavna železnička stanica");
	m_resenjeAsocijacije.append("zeleznicka stanica");
	m_resenjeAsocijacije.append("glavna zeleznicka stanica");
	m_resenjeAsocijacije.append("zeleznicka");
	m_resenjeAsocijacije.append("železnička");
	m_hint = "ću ću";
	m_naslovEdukativnogTeksta = "O Gardoš kuli";
}
