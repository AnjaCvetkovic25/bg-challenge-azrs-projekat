// zeleznickastanica.cpp

#include "../include/zeleznickastanica.h"
#include <QDebug>
#include <QPainter>

#include <QBrush>
#include <QDialog>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPushButton>
#include <QString>
#include <QUiLoader>

ZeleznickaStanica::ZeleznickaStanica(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(5, 2, vreme, pocetnoVreme, parent)
{

	this->setWindowTitle("BgChallenge");
	this->setMinimumWidth(1280);
	this->setMinimumHeight(720);
	this->setFixedSize(this->width(), this->height());

	this->setImagePath(":/new/pozadine/resources/vozovi(1).png");
	this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

	QFile file(":/new/resources/forms/zeleznicka.ui");

	if (!file.open(QFile::ReadOnly))
	{
		qDebug() << "Error opening UI file zeleznicka.ui:" << file.errorString();
		return;
	}

	QUiLoader loader;
	ui = loader.load(&file, this);
	ui->setFixedSize(this->width(), this->height());
	file.close();

	QLCDNumber* lcdNumber = ui->findChild<QLCDNumber*>("lcdNumber");

	connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	connect(ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &ZeleznickaStanica::zatvori);
	connect(ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &ZeleznickaStanica::pomoc);

	Tekstovi();

	// Deklaracija dugmadi
	QPushButton* btnRomantika = ui->findChild<QPushButton*>("btnRomantika");
	QPushButton* btnCrveni = ui->findChild<QPushButton*>("btnCrveni");
	QPushButton* btnNostalgija = ui->findChild<QPushButton*>("btnNostalgija");
	QPushButton* btnPlavi = ui->findChild<QPushButton*>("btnPlavi");

	QPushButton* btnProveri = ui->findChild<QPushButton*>("btnProveri");

	QPushButton* levoLjubljana = ui->findChild<QPushButton*>("levoLjubljana");
	QPushButton* desnoLjubljana = ui->findChild<QPushButton*>("desnoLjubljana");

	QPushButton* levoZagreb = ui->findChild<QPushButton*>("levoZagreb");
	QPushButton* desnoZagreb = ui->findChild<QPushButton*>("desnoZagreb");

	QPushButton* levoJugoslavija = ui->findChild<QPushButton*>("levoJugoslavija");
	QPushButton* desnoJugoslavija = ui->findChild<QPushButton*>("desnoJugoslavija");

	QPushButton* levoMokraGora = ui->findChild<QPushButton*>("levoMokraGora");
	QPushButton* desnoMokraGora = ui->findChild<QPushButton*>("desnoMokraGora");

	connect(levoLjubljana, &QPushButton::clicked, this, &ZeleznickaStanica::levoLjubljana_clicked);
	connect(desnoLjubljana, &QPushButton::clicked, this, &ZeleznickaStanica::desnoLjubljana_clicked);

	connect(levoMokraGora, &QPushButton::clicked, this, &ZeleznickaStanica::levoMokraGora_clicked);
	connect(desnoMokraGora, &QPushButton::clicked, this, &ZeleznickaStanica::desnoMokraGora_clicked);

	connect(levoJugoslavija, &QPushButton::clicked, this, &ZeleznickaStanica::levoTito_clicked);
	connect(desnoJugoslavija, &QPushButton::clicked, this, &ZeleznickaStanica::desnoTito_clicked);

	connect(levoZagreb, &QPushButton::clicked, this, &ZeleznickaStanica::levoZagreb_clicked);
	connect(desnoZagreb, &QPushButton::clicked, this, &ZeleznickaStanica::desnoZagreb_clicked);

	connect(btnProveri, &QPushButton::clicked, this, &ZeleznickaStanica::btnProveri_clicked);

	btnRomantika->setEnabled(false);
	btnCrveni->setEnabled(false);
	btnNostalgija->setEnabled(false);
	btnPlavi->setEnabled(false);

	odgovori = QList<QString>();
	odgovori.push_back("Cela Jugoslavija");
	odgovori.push_back("Ljubljana");
	odgovori.push_back("Mokra Gora");
	odgovori.push_back("Zagreb");

	tito = ui->findChild<QPushButton*>("tbJugoslavija");
	;
	mokraGora = ui->findChild<QPushButton*>("tbMokraGora");
	;
	zagreb = ui->findChild<QPushButton*>("tbZagreb");
	;
	ljubljana = ui->findChild<QPushButton*>("tbLjubljana");
	;

	tito->setEnabled(false);
	mokraGora->setEnabled(false);
	zagreb->setEnabled(false);
	ljubljana->setEnabled(false);

	brojacTito = new int(-1);
	brojacLjubljana = new int(-1);
	brojacMokraGora = new int(-1);
	brojacZagreb = new int(-1);
}

void ZeleznickaStanica::pomoc()
{

	QDialog dijalog;
	dijalog.setFixedHeight(300);
	dijalog.setFixedWidth(400);
	dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
						  "rgb(200,200,200); margin: 10px;}");

	QLabel* text = new QLabel("Srce koje je nekada povezivalo krajeve bivše Jugoslavije bila je "
							  "Železnička stanica, pre nego što se preselila na Prokop. Mesto koje "
							  "sada služi za filmske setove zbog svog enterijera poput vremenske "
							  "kapsule, nekada je bilo žarište putovanja. Tada su postojali ikonski "
							  "vozovi čije je ime ostalo samo u sećanju. Da li možete da pretpostavite "
							  "koji voz je išao kuda? Znate li koji voz je bio čuveni Titov voz koji "
							  "ga je razvozio gde god je trebalo poći?",
							  &dijalog);
	text->setAlignment(Qt::AlignCenter);
	text->setWordWrap(true);
	text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
	QPushButton* ok = new QPushButton("OK", &dijalog);
	ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
					  "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
					  "padding: 10px; border-radius: 21px; font: bold 14px;}"
					  "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
					  "#bcbcbc; }"
					  "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
					  "#a0a0a0; }");
	QVBoxLayout* layout = new QVBoxLayout(&dijalog);

	layout->addWidget(text);

	layout->addWidget(ok);
	connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
	dijalog.exec();
}

void ZeleznickaStanica::levoLjubljana_clicked()
{
	// ljubljana->setStyleSheet("");

	int pomeriLevo = *brojacLjubljana - 1;

	if (pomeriLevo < 0)
	{
		pomeriLevo = (pomeriLevo + 4) % 4;
	}

	ljubljana->setText(odgovori[pomeriLevo]);

	*brojacLjubljana = pomeriLevo;
}

void ZeleznickaStanica::desnoLjubljana_clicked()
{
	// ljubljana->setStyleSheet("");
	int pomeriDesno = *brojacLjubljana + 1;

	if (pomeriDesno > 3)
	{
		pomeriDesno = (pomeriDesno + 4) % 4;
	}

	ljubljana->setText(odgovori[pomeriDesno]);

	*brojacLjubljana = pomeriDesno;
}

void ZeleznickaStanica::levoMokraGora_clicked()
{
	// mokraGora->setStyleSheet("");
	int pomeriLevo = *brojacMokraGora - 1;

	if (pomeriLevo < 0)
	{
		pomeriLevo = (pomeriLevo + 4) % 4;
	}

	mokraGora->setText(odgovori[pomeriLevo]);

	*brojacMokraGora = pomeriLevo;
}

void ZeleznickaStanica::desnoMokraGora_clicked()
{
	// mokraGora->setStyleSheet("");
	int pomeriDesno = *brojacMokraGora + 1;

	if (pomeriDesno > 3)
	{
		pomeriDesno = (pomeriDesno + 4) % 4;
	}

	mokraGora->setText(odgovori[pomeriDesno]);

	*brojacMokraGora = pomeriDesno;
}

void ZeleznickaStanica::levoTito_clicked()
{
	// tito->setStyleSheet("");
	int pomeriLevo = *brojacTito - 1;

	if (pomeriLevo < 0)
	{
		pomeriLevo = (pomeriLevo + 4) % 4;
	}

	tito->setText(odgovori[pomeriLevo]);

	*brojacTito = pomeriLevo;
}

void ZeleznickaStanica::desnoTito_clicked()
{
	// tito->setStyleSheet("");
	int pomeriDesno = *brojacTito + 1;

	if (pomeriDesno > 3)
	{
		pomeriDesno = (pomeriDesno + 4) % 4;
	}

	tito->setText(odgovori[pomeriDesno]);

	*brojacTito = pomeriDesno;
}

void ZeleznickaStanica::levoZagreb_clicked()
{
	// zagreb->setStyleSheet("");
	int pomeriLevo = *brojacZagreb - 1;

	if (pomeriLevo < 0)
	{
		pomeriLevo = (pomeriLevo + 4) % 4;
	}

	zagreb->setText(odgovori[pomeriLevo]);

	*brojacZagreb = pomeriLevo;
}

void ZeleznickaStanica::desnoZagreb_clicked()
{
	// zagreb->setStyleSheet("");

	int pomeriDesno = *brojacZagreb + 1;

	if (pomeriDesno > 3)
	{
		pomeriDesno = (pomeriDesno + 4) % 4;
	}

	zagreb->setText(odgovori[pomeriDesno]);

	*brojacZagreb = pomeriDesno;
}

void ZeleznickaStanica::btnProveri_clicked()
{

	if (ljubljana->text().compare("Ljubljana") == 0)
	{
		ljubljana->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
								 "solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}
	else
	{
		ljubljana->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
								 "solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}

	if (zagreb->text().compare("Zagreb") == 0)
	{
		zagreb->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
							  "solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}
	else
	{
		zagreb->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
							  "solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}

	if (mokraGora->text().compare("Mokra Gora") == 0)
	{
		mokraGora->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
								 "solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}
	else
	{
		mokraGora->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
								 "solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}

	if (tito->text().compare("Cela Jugoslavija") == 0)
	{
		tito->setStyleSheet("QPushButton { background-color: rgba(46,194,126,80%); border: 1px "
							"solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}
	else
	{
		tito->setStyleSheet("QPushButton { background-color: rgba(237,51,59,80%); border: 1px "
							"solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");
	}

	if ((ljubljana->text().compare("Ljubljana") == 0) && (zagreb->text().compare("Zagreb") == 0) &&
		(mokraGora->text().compare("Mokra Gora") == 0) && (tito->text().compare("Cela Jugoslavija") == 0))
	{
		foreach (QObject* child, ui->children())
		{
			if (child->isWidgetType())
			{
				QWidget* childWidget = qobject_cast<QWidget*>(child);
				if (childWidget)
				{
					qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

					childWidget->hide();
				}
			}
		}

		if (this->layout() != nullptr)
		{
			QLayoutItem* item;
			while ((item = this->layout()->takeAt(0)) != nullptr)
			{
				qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

				item->widget()->setVisible(false);
				delete item;
			}
			delete this->layout();
		}

		QLayout* postojeciLayout = this->layout();
		if (postojeciLayout)
		{
			delete postojeciLayout;
		}

		this->prikaziEdukativniTekst();
	}
}

ZeleznickaStanica::~ZeleznickaStanica()
{
	delete brojacTito;
	delete brojacMokraGora;
	delete brojacZagreb;
	delete brojacLjubljana;
	delete ui;
}

void ZeleznickaStanica::Tekstovi()
{

	m_edukativniTekst = R"(
Zgrada je izdradjena po uzoru na zeleznicke stanice velikih evropskih zemalja i stoji kao monumetalno zdanje. Trebalo je odrediti mesto novoj stanici.
Francuzi su predlagali Prokop. Srpski članovi komisije su taj predlog odbacili jer bi onda stanica bila suviše udaljena od grada. Oni su hteli da to važno i veliko zdanje bude urbanistički svetionik (francuski predlog će biti usvojen skoro vek kasnije). Ideja da železnička stanica bude ukras gradu obilato je prihvaćena u javnosti.
Ulaz stanice odrediće pravac Nemanjinog bulevara: u vreme kad je stanica građena ne samo što tu nije bilo ulice, nego ni ledine. Bara Venecija je bio šaljivi naziv za močvaru koja se kilometrima pružala uz Savu, pa sve uz breg do Topčiderskog puta (ulice Kneza Miloša).

1941. iz nemačkih aviona u ponirućem letu i uz pištanje sirena na stanicu su izbačeni tovari bombi. Taman je obnovljena, da bi u aprilu 1944. na nju, sa 5000m visine, poletele polutonske bombe iz američkih i engleskih bombardera.

U Titovoj Jugoslaviji stanica je zguljeno obnovljena. Kule na peronskom ulazu nisu ponovo podignute, pa je narušen opšti utisak stanice. Međutim, danas možemo zateći, po nekim mišljenjima grotesknu, statuu Stefana Nemanje.

Sredinom 2018. železnička stanica je zatvorena i napuštena. Postala je žrtva svoje skupe lokacije namenjene izgradnji kvarta solitera pored Save. Vozovi su užurbano izmešteni u nedovršenu stanicu Prokop.

Čast da poslednji pođe sa Glavne stanice pripala je međunarodnom vozu koji je krenuo 30. juna u 21.40h. Kao da mu je određena simbolična putanja, išao je za Budimpeštu, kuda i prvi, koji je sa iste stanice krenuo pre 134 godine. Prvi voz za Evropu je ispraćen marševima Orkestra Kraljevske garde, a poslednji je ispraćen pesmom A sad adio.
Od kultnih vozova, koji se danas mogu posetiti kao muzeji, tu su Romantika (čija je poslednja destinacija bila Ljubljana), Nostalgija (redovno prevozila do Mokre Gore), Crveni voz koji je išao do Zagreba, i najpoznatiji, Titov Plavi voz, koji ga je vozio gde god je trebalo da se krene, širom Jugoslavije i Evrope. )";

	m_asocijacija = "100 dinara";

	m_hint = "Ako ne znaš ko se nalazi na novčanici od 100 dinara, otvori "
			 "novčanik. Ukoliko imaš išta keša, studentska sirotinjo. I imaj na "
			 "umu da tražimo lokaciju - zgradu - od tebe.";

	m_resenjeAsocijacije.append("muzej nikole tesle");

	m_naslovEdukativnogTeksta = "O Glavnoj železničkoj stanici";
}
