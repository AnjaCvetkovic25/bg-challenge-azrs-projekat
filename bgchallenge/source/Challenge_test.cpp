#include "../include/Challenge_test.h"
#include <QDebug>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPixmap>
#include <QPushButton>
#include <QSpacerItem>
#include <QString>
#include <QTextBrowser>

Challenge_test::Challenge_test(int id, int nivo, Timer* vreme, int pocetnoVreme, QWidget* parent)
	: m_id(id), m_nivo(nivo), m_vreme(vreme), m_pocetnoVreme(pocetnoVreme), QWidget(parent)
{
}

void Challenge_test::zapocniChallenge()
{
}
void Challenge_test::zavrsiChallenge()
{
}

Challenge_test::~Challenge_test()
{
}

void Challenge_test::setImagePath(const QString& newImagePath)
{
	m_imagePath = newImagePath;
	update();
}

void Challenge_test::paintEvent(QPaintEvent* event)
{
	Q_UNUSED(event);

	QPainter painter(this);

	QPixmap backgroundImage(m_imagePath);

	painter.drawPixmap(rect(), backgroundImage, backgroundImage.rect());
}

void Challenge_test::zatvori()
{
	qDebug() << m_id << "closed";
	this->close();
	emit closed();
}

void Challenge_test::prikaziEdukativniTekst()
{
	if (m_id < 16)
	{

		// this->setImagePath(":/new/resources/resources/cropped_map.png");
		QTextBrowser* textBrowserEdukativniTekst = new QTextBrowser(this);
		textBrowserEdukativniTekst->setText(m_edukativniTekst);
		QLabel* labelAsocijacija = new QLabel(m_asocijacija);
		QPushButton* pbHint = new QPushButton("Pomoc");
		QPushButton* pbOdgovor = new QPushButton("Odgovori");
		QLineEdit* lineEdit = new QLineEdit;
		QLabel* labelIme = new QLabel(m_naslovEdukativnogTeksta);
		QLabel* labelSledece = new QLabel("Naredna lokacija:");
		QSpacerItem* hSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);
		// QPushButton* pbNazad = new QPushButton("Nazad na mapu"); //nema nazad sta
		// oces

		QGridLayout* layout = new QGridLayout(this);
		layout->addWidget(labelIme, 0, 0, 1, 3);

		layout->addWidget(textBrowserEdukativniTekst, 1, 0, 2, 3);
		layout->addItem(hSpacer, 3, 0);
		layout->addWidget(labelSledece, 4, 0, 1, 3);
		layout->addWidget(labelAsocijacija, 5, 0);
		layout->addWidget(pbHint, 5, 2);
		layout->addWidget(lineEdit, 6, 0);
		layout->addWidget(pbOdgovor, 6, 2);

		QString btnStyleSheet = "QPushButton { background-color: #f0f0f0; border: 1px solid #dcdcdc; "
								"color: #333; padding: 10px; border-radius: 21px; font: bold "
								"14px;width: 100px;}"
								"QPushButton:hover { background-color: #e0e0e0; border: 1px solid "
								"#bcbcbc; }"
								"QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
								"#bcbcbc; }"
								"QPushButton:pressed {background-color: #d0d0d0; border: 1px solid "
								"#a0a0a0; }";
		pbOdgovor->setStyleSheet(btnStyleSheet);
		pbHint->setStyleSheet(btnStyleSheet);

		textBrowserEdukativniTekst->setStyleSheet(
			"QTextBrowser { text-align: center; padding: 10px; color: black; font: "
			"bold 18px; background-color: rgba(200, 200, 200, 85%); border-radius: "
			"5px;}");
		textBrowserEdukativniTekst->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		// textBrowserEdukativniTekst->setMaximumSize(1280, 720);
		textBrowserEdukativniTekst->setMinimumHeight(500);
		textBrowserEdukativniTekst->setAlignment(Qt::AlignCenter);
		// textBrowserEdukativniTekst->setWordWrap(true);
		labelAsocijacija->setStyleSheet("QLabel {padding: 10px; color: #333; font: bold 16px; "
										"background-color: rgba(200, 200, 200, 80%); border-radius: 5px;}");

		lineEdit->setStyleSheet("QLineEdit { background-color: rgba(200, 200, 200, 50%); border: 1px "
								"solid #ccc; border-radius: 10px; padding: 8px; font: bold 14px; }");

		labelIme->setStyleSheet("QLabel {font: bold 30px; background-color: "
								"rgba(0,0,0,30%); border-radius: 5px}");
		labelIme->setAlignment(Qt::AlignCenter);

		labelSledece->setStyleSheet("QLabel {padding: 10px; font: bold 21px; background-color: rgba(190, "
									"190, 190, 50%); border-radius: 5px}");
		labelSledece->setAlignment(Qt::AlignLeft);

		this->setLayout(layout);

		connect(pbHint, &QPushButton::clicked, this, [=]() { lineEdit->setText(m_hint); });

		connect(pbOdgovor, &QPushButton::clicked, this, [=]() {
			QString odg = lineEdit->text();
			lineEdit->clear();

			if (m_resenjeAsocijacije.contains(odg.toLower()))
			{
				emit resenIzazov(m_id);
				this->close();
				emit closed();
			}
		});
	}
	else
	{

		// this->setImagePath(":/new/resources/resources/cropped_map.png");
		QLabel* labelIme = new QLabel(m_naslovEdukativnogTeksta);
		QLabel* labelEdukativniTekst = new QLabel(m_edukativniTekst);
		QPushButton* pbDalje = new QPushButton("Dalje");
		QSpacerItem* vSpacer = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

		QGridLayout* layout = new QGridLayout(this);
		layout->addWidget(labelIme, 0, 0, 1, 3);
		layout->addWidget(labelEdukativniTekst, 1, 0, 2, 3);
		layout->addItem(vSpacer, 3, 2);
		layout->addWidget(pbDalje, 4, 2);

		QString btnStyleSheet = "QPushButton { background-color: #f0f0f0; border: 1px solid #dcdcdc; "
								"color: #333; padding: 10px; border-radius: 21px; font: bold "
								"14px;width: 100px;}"
								"QPushButton:hover { background-color: #e0e0e0; border: 1px solid "
								"#bcbcbc; }"
								"QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
								"#bcbcbc; }"
								"QPushButton:pressed {background-color: #d0d0d0; border: 1px solid "
								"#a0a0a0; }";
		pbDalje->setStyleSheet(btnStyleSheet);

		labelEdukativniTekst->setStyleSheet("QLabel {padding: 20px; margin: 10px; color: #333; font: bold 16px; "
											"background-color: rgba(192, 192, 192, 70%); border-radius: 5px;}");
		labelEdukativniTekst->setWordWrap(true);

		labelIme->setStyleSheet("QLabel {font: bold 30px; background-color: "
								"rgba(0,0,0,30); border-radius: 5px}");
		labelIme->setAlignment(Qt::AlignCenter);

		this->setLayout(layout);

		connect(pbDalje, &QPushButton::clicked, this, [=]() {
			emit resenIzazov(16);
			this->close();
			emit closed();
		});
	}
}
