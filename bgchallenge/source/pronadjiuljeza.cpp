#include "../include/pronadjiuljeza.h"
#include "../forms/ui_pronadjiuljeza.h"
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPainter>
#include <QPainterPath>
#include <QPixmap>

PronadjiUljeza::PronadjiUljeza(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(13, 4, vreme, pocetnoVreme, parent), ui(new Ui::PronadjiUljeza)
{
	ui->setupUi(this);
	Tekstovi();

	QPixmap limunzut(":/new/nadjiuljeza/resources/kafana_limun_zut.jpg");
	QPixmap malivrabac(":/new/nadjiuljeza/resources/mali_vrabac.png");
	QPixmap trisesira(":/new/nadjiuljeza/resources/kafana_tri_sesira.png");
	QPixmap znakpitanja(":/new/nadjiuljeza/resources/znak_pitanja.png");
	QPixmap zlatnibokal(":/new/nadjiuljeza/resources/kafana_zlatni_bokal.jpg");
	QPixmap sesirmoj(":/new/nadjiuljeza/resources/sesir_moj.jpg");

	buttonMask(ui->limunzut, limunzut);
	buttonMask(ui->malivrabac, malivrabac);
	buttonMask(ui->trisesira, trisesira);
	buttonMask(ui->znakpitanja, znakpitanja);
	buttonMask(ui->zlatnibokal, zlatnibokal);
	buttonMask(ui->sesirmoj, sesirmoj);

	setImagePath(":/new/pozadine/resources/skadarlija.jpg");
	connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		ui->lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	connect(ui->pbNazad, &QPushButton::clicked, this, &PronadjiUljeza::zatvori);

	QTimer::singleShot(0, this, &PronadjiUljeza::Info);
	connect(ui->znakpitanja, &QPushButton::clicked, this, &PronadjiUljeza::znakpitanja_clicked);
	connect(ui->limunzut, &QPushButton::clicked, this, &PronadjiUljeza::limunzut_clicked);
	connect(ui->zlatnibokal, &QPushButton::clicked, this, &PronadjiUljeza::zlatnibokal_clicked);
	connect(ui->trisesira, &QPushButton::clicked, this, &PronadjiUljeza::trisesira_clicked);
	connect(ui->malivrabac, &QPushButton::clicked, this, &PronadjiUljeza::malivrabac_clicked);
	connect(ui->sesirmoj, &QPushButton::clicked, this, &PronadjiUljeza::sesirmoj_clicked);
}

PronadjiUljeza::~PronadjiUljeza()
{
	delete ui;
}

void PronadjiUljeza::buttonMask(QPushButton* button, const QPixmap& image)
{
	QPainterPath rounded;

	rounded.addRoundedRect(button->rect(), button->property("borderRadius").toDouble(),
						   button->property("borderRadius").toDouble());

	QPixmap scaled = image.scaled(button->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

	button->setMask(rounded.toFillPolygon().toPolygon());

	QPixmap maskedImage(scaled);
	maskedImage.fill(Qt::transparent);
	QPainter painter(&maskedImage);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setClipPath(rounded);
	painter.drawPixmap(0, 0, scaled);

	button->setIcon(QIcon(maskedImage));
	button->setIconSize(button->size());
}

void PronadjiUljeza::znakpitanja_clicked()
{
	QString tekst = "                                              NETAČNO!\n\n"
					"Godine 1823. jedan od glavnih odrganizatora Prvog srpskog ustanka, Naum "
					"Ičko, podigao je kuću u kojoj se nalazi kafana \"?\", koja je tada bila "
					"svojina kneza Miloša. Kafana „? „(„Znak pitanja“/ „Upitnik“) jedna je "
					"od najstarijih kuća u Beogradu i najstarija sačuvana beogradska kafana.";

	QMessageBox msgBox;
	msgBox.setWindowTitle("Kafana Znak pitanja");
	msgBox.setText(tekst);
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);

	QPushButton* okButton = msgBox.addButton(QMessageBox::Ok);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	okButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();
	msgBox.setFixedSize(400, 400);
	znakpitanja = true;
}

void PronadjiUljeza::zlatnibokal_clicked()
{
	QString tekst = "                                              NETAČNO!\n\n"
					"U 19. veku otvorena je kafana „Zlatni bokal“ u Skadarliji, prvobitno "
					"zamišljena kao vinski podrum. Danas, zahvaljujući baš restoranu „Zlatni "
					"bokal“ Skadarlija može da se pohvali da je zadržala staru tradiciju, "
					"jer je upravo on jedan od nekoliko najstarijih i najautentičnijih "
					"restorana koji još uvek rade.";

	QMessageBox msgBox;
	msgBox.setWindowTitle("Kafana Zlatni bokal");
	msgBox.setText(tekst);
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);

	QPushButton* okButton = msgBox.addButton(QMessageBox::Ok);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	okButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();
	zlatnibokal = true;
}

void PronadjiUljeza::trisesira_clicked()
{
	QString tekst = "                                              NETAČNO!\n\n"
					"Kafana “Tri šešira” otvorena je u kući u kojoj je prethodno bila "
					"zanatska radionica, čiji su zaštitni znak bila tri plehana šešira. "
					"Mnogi poznati beogradski pisci i umetnici, poput Branislava Nušića, Tin "
					"Ujevića, Čiča Ilije Stanojevića, Rake Drainca, Đure Jakšića bili su "
					"česti posetioci “Tri šešira” a neki od njih,  poput pomenutog Jakšića "
					"provodili su vreme više u kafani nego u svojoj kući.";

	QMessageBox msgBox;
	msgBox.setWindowTitle("Kafana Tri šešira");
	msgBox.setText(tekst);
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);

	QPushButton* okButton = msgBox.addButton(QMessageBox::Ok);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	okButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();
	trisesira = true;
}

void PronadjiUljeza::malivrabac_clicked()
{
	QString tekst = "                                              NETAČNO!\n\n"
					"Kafana Mali Vrabac nalazi se u samom srcu Beograda, u boemskoj četvrti "
					"Skadarlija.Sređen po uzoru na čaršiju iz 19. veka, odiše neverovatnim "
					"pozitivnim duhom i tradicijom Srbije koja je bila u usponu.";

	QMessageBox msgBox;
	msgBox.setWindowTitle("Kafana Mali vrabac");
	msgBox.setText(tekst);
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);

	QPushButton* okButton = msgBox.addButton(QMessageBox::Ok);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	okButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();
	malivrabac = true;
}

void PronadjiUljeza::sesirmoj_clicked()
{
	QString tekst = "                                              NETAČNO!\n\n"
					"Šešir moj čuvena je skadarlijska kafana, koja je relativno "
					"„mlada“, osnovana 1990. godine. No, to je nije sprečilo da "
					"u samo četvrt stoleća postojanja stekne reputaciju jedne od "
					"tradicionalnih kafana, gde se susreću duh Skadarlije, ritam "
					"Beograda, okusi Srbije i gosti iz celog sveta.";

	QMessageBox msgBox;
	msgBox.setWindowTitle("Kafana Šešir moj");
	msgBox.setText(tekst);
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);

	QPushButton* okButton = msgBox.addButton(QMessageBox::Ok);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	okButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();
	sesirmoj = true;
}

void PronadjiUljeza::limunzut_clicked()
{
	QString tekst = "                                             TAČNO!\n\n"
					"Ovo je jedina kafana koja se ne nalazi u Skadarliji.\nKafana Limun Žut "
					"se nalazi u širem centru Beograda u neposrednoj blizini Vukovog "
					"spomenika, u ulici Dimitrija Tucovića na Zvezdari. Utorkom i četvrtkom "
					"možete da pijete vrhunsku rakju za samo 99 dinara :).";

	QMessageBox msgBox;
	msgBox.setWindowTitle("Kafana Šešir moj");
	msgBox.setText(tekst);
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);

	QAbstractButton* daljeButton = msgBox.addButton("Dalje", QMessageBox::ActionRole);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	daljeButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();
	if (msgBox.clickedButton() == daljeButton)
	{
		if (this->layout() != nullptr)
		{
			QLayoutItem* item;
			while ((item = this->layout()->takeAt(0)) != nullptr)
			{
				delete item->widget();
				delete item;
			}
			delete this->layout();
		}
		foreach (QObject* child, this->children())
		{
			if (child->isWidgetType())
			{
				QWidget* childWidget = qobject_cast<QWidget*>(child);
				if (childWidget)
				{
					childWidget->hide();
				}
			}
		}
		prikaziEdukativniTekst();
	}
}

void PronadjiUljeza::Tekstovi()
{

	m_edukativniTekst = R"("
Beogradska četvrt u srcu srpske prestonice privlači pažnju svakog turiste. Što zbog mnoštva kafana, što zbog kaldrme kojom je prekrivena, Skadarska ulica spada u red najposećenijih beogradskih znamenitosti. Ulica duga ni šest stotina metara, jedna je od najpoznatijih u Beogradu. Posebno je atraktivna u toku večeri i u dane vikenda.
Podno nekadašnjeg Pozorišnog trga, koji se nalazio na mestu današnjeg Trga republike, ugnezdila se čuvena boemska četvrt, koju su posećivali poznati pisci, glumci i ljubitelji čašice, ali i dobrog provoda. U Skadarskoj ulici živeo je i naš pesnik Đura Jakšić. Čuvena je anegdota o jednom od Đurinih pokušaja da se oslobodi poroka i da ne popije ni kap alkohola dok prolazi kroz Skadarsku, na putu prema centru tadašnjeg Beograda. Bio je karakter i nije svratio ni u jednu od brojnih skadarlijskih kafana. Pa kako red nalaže, kada je stigao do one na vrhu, rešio je da počasti sebe i svrati, jer je "bio karakter" i čitavih nekoliko stotina metara nije popio ni kap alkohola.

Duh Beograda najbolje se oseti tokom šetnje kroz ulicu, koja je prvi put u plan Grada Beograda ucrtana 1854. , mada je ona postojala još od 1720.
Ovo je jedna od retkih beogradskih ulica koja vekovima nosi isti naziv. Najpre nazvana Ružina ulica, Skadarska postaje nedugo nakon oslobođenja od austougarske vlasti i taj naziv se zadržao do danas.

Interesantan podatak potiče iz 1892. godine, kada je na mestu "Male pivare" izgrađena čuvena Bajlonijeva pivara, po kojoj je obližnja pijaca dobila ime. Naime tada je prilikom kopanja temelja pronađena lobanja Homo primagenijus-a, od milošte nazvanog "prvi Beograđanin". Njemu se tokom Prvog svetskog rata (ili Velikog rata kako su ga tada zvali), izgubio svaki trag. )";

	m_asocijacija = "Kod Konja";

	m_hint = "Plato ispred Narodnog Pozorišta";

	m_resenjeAsocijacije.append("trg republike");
	m_naslovEdukativnogTeksta = "O Skadarliji";
}

void PronadjiUljeza::Info()
{
	QString tekst = "                                 Challenge pronađi uljeza\n\n"
					"Uputstvo:\nVaš zadatak je da, među šest ponuđenih logotipa kafana, "
					"pronađete onaj koji neodgovara i predstavlja uljeza. Srećno!";

	QMessageBox msgBox;
	msgBox.setWindowTitle(" ");
	msgBox.setText(tekst);
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";
	msgBox.setStyleSheet(msgBoxStylesheet);

	QPushButton* okButton = msgBox.addButton(QMessageBox::Ok);
	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	okButton->setStyleSheet(buttonStylesheet);
	msgBox.exec();
}
