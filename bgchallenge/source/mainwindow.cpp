#include "../include/mainwindow.h"
#include "../forms/ui_mainwindow.h"
#include "../include/challengeMorzeovaAzbukaUI.h"
#include "../include/igrac.h"
#include "../include/mapa.h"
#include "../include/rezultat.h"
#include <QDebug>
#include <QFile>
#include <QInputDialog>
#include <QLineEdit>
#include <QMessageBox>
#include <QPalette>
#include <QTabWidget>
#include <QTextStream>
#include <QTime>
#include <QUiLoader>
#include <QVector>

mainwindow::mainwindow(QWidget* parent) : QWidget(parent), ui(new Ui::mainwindow)
{
	ui->setupUi(this);
	this->setWindowTitle("BgChallenge");
	this->setFixedSize(this->width(), this->height());
	this->setUpBackground();

	rez = new Rezultat();
	rez->ucitajIgraceJson(":/new/resources/resources/savefile.json");

	this->showLoadGame();
	// loadScores();
	this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));
	ui->stackedWidget->setCurrentIndex(0);

	qApp->installEventFilter(this);

	connect(ui->pbNapustiIgricu, SIGNAL(clicked()), this, SLOT(NapustiIgricuClicked()));
	connect(ui->pbZapocniIgricu, SIGNAL(clicked()), this, SLOT(ZapocniIgricuClicked()));
	connect(ui->pbNazad_2, SIGNAL(clicked()), this, SLOT(NatragNaGlavniMeniClicked()));
	connect(ui->pbPregledRezultata, &QPushButton::clicked, this, &mainwindow::PregledRezultataClicked);
	connect(ui->pbNazadNaMeni, &QPushButton::clicked, this, &mainwindow::NatragNaGlavniMeniClicked);
	connect(ui->pbNazad, &QPushButton::clicked, this, &mainwindow::NatragNaGlavniMeniClicked);
	connect(ui->pbPodesavanja, &QPushButton::clicked, this, &mainwindow::PodesavanjaClicked);
	setUpTabWidges();
	connect(ui->pbNastaviIgru, SIGNAL(clicked()), this, SLOT(NastaviIgricuClicked()));
	connect(ui->pbKakoSeIgra, &QPushButton::clicked, this, &mainwindow::PrikaziUputstvo);

	connect(ui->pbKreni, &QPushButton::clicked, this, &mainwindow::Kreni);

	QObject::connect(this, &mainwindow::clicked, this, &mainwindow::KlikMelodija);
	connect(ui->RB_ukljucen, &QRadioButton::clicked, this, &mainwindow::KlikMelodija);
	connect(ui->HS_jacina_tona, &QSlider::valueChanged, this, &mainwindow::JacinaPozadinseMelodije);

	player = new QMediaPlayer();
	audioOutput = new QAudioOutput();
	player->setAudioOutput(audioOutput);
	player->setSource(QUrl("qrc:/new/resources/resources/mixkit-trap-hamza-267.mp3"));
	audioOutput->setVolume(0);
	player->play();

	player_klik = new QMediaPlayer();
	audioOutput_klik = new QAudioOutput();
}

mainwindow::~mainwindow()
{
	if (rez != nullptr)
	{
		delete rez;
		rez = nullptr;
	};

	if (mapa != nullptr)
	{
		qDebug() << "obrisana mapa";
		delete mapa;
		mapa = nullptr;
	};

	if (player != nullptr)
	{
		qDebug() << "obrisan player";
		delete player;
		player = nullptr;
	};

	if (audioOutput != nullptr)
	{
		qDebug() << "obrisan audioOutput";
		delete audioOutput;
		audioOutput = nullptr;
	};

	if (player_klik != nullptr)
	{
		qDebug() << "obrisan player_klik";
		delete player_klik;
		player_klik = nullptr;
	};

	if (audioOutput_klik != nullptr)
	{
		qDebug() << "obrisan audioOutput_klik";
		delete audioOutput_klik;
		audioOutput_klik = nullptr;
	};

	delete ui;
}

void mainwindow::ZapocniIgricuClicked()
{
	if (mapa != nullptr)
	{
		qDebug() << "obrisana mapa";
		delete mapa;
		mapa = nullptr;
	}
	ui->stackedWidget->setCurrentIndex(1);
}

void mainwindow::Kreni()
{
	QString ime = ui->lineEdit->text();
	if (ime == "" || postojiIgrac(ime))
	{
		ui->lineEdit->clear();
		QMessageBox::warning(this, "Uneto ime postoji!", "Molimo unesite drugo ime!");
	}
	else
	{
		ui->lineEdit->clear();
		Igrac noviIgrac(ime);
		rez->dodajIgraca(noviIgrac);
		rez->sacuvajIgraceJson("../bgchallenge/resources/savefile.json");
		if (mapa != nullptr)
		{
			qDebug() << "obrisana mapa";
			delete mapa;
			mapa = nullptr;
		}
		mapa = new MapaUI(noviIgrac, rez, this);
		mapa->raise();
		mapa->show();
		connect(mapa, &MapaUI::cestitajIgracu, this, &mainwindow::cestitajIgracu);
		ui->stackedWidget->setCurrentIndex(0);
	}
}

bool mainwindow::postojiIgrac(const QString& ime)
{
	return rez->postojiIgrac(ime);
}

void mainwindow::NastaviIgricuClicked()
{
	bool ok;
	QString ime = QInputDialog::getText(this, "Nastavi igru", "Molimo unesite ime:", QLineEdit::Normal, QString(), &ok);

	if (ok && !ime.isEmpty())
	{

		if (postojiIgrac(ime))
		{
			Igrac stariIgrac = rez->nadjiIgraca(ime);
			qDebug() << stariIgrac.getIme() << stariIgrac.getVreme();
			if (mapa != nullptr)
			{
				qDebug() << "obrisana mapa";
				delete mapa;
				mapa = nullptr;
			};
			mapa = new MapaUI(stariIgrac, rez, this);
			mapa->raise();
			mapa->show();
			connect(mapa, &MapaUI::cestitajIgracu, this, &mainwindow::cestitajIgracu);
			ui->stackedWidget->setCurrentIndex(0);
			showLoadGame();
		}
		else
		{
			QMessageBox::warning(this, "Uneli ste nepostojece ime",
								 "Molimo zapocnite novu igru ili unesite ispravno ime!");
		}
	}
	else
	{
		QMessageBox::warning(this, "Upozorenje", "Niste uneli ime!");
	}
}

void mainwindow::PregledRezultataClicked()
{
	ui->stackedWidget->setCurrentIndex(3);
	setUpTabWidges();
}

void mainwindow::PodesavanjaClicked()
{
	ui->stackedWidget->setCurrentIndex(4);
}

void mainwindow::NapustiIgricuClicked()
{
	QMessageBox msgBox;
	msgBox.setText("Da li zaista želite da napustite igricu?");

	QAbstractButton* daButton = msgBox.addButton("Da", QMessageBox::YesRole);
	QAbstractButton* neButton = msgBox.addButton("Ne", QMessageBox::NoRole);

	// Ukloni standardne dugmadi za zatvaranje i minimizaciju
	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";

	msgBox.setStyleSheet(msgBoxStylesheet);

	daButton->setStyleSheet(buttonStylesheet);
	neButton->setStyleSheet(buttonStylesheet);

	msgBox.exec();

	if (msgBox.clickedButton() == daButton)
	{
		qApp->quit();
	}
}

void mainwindow::NatragNaGlavniMeniClicked()
{
	ui->stackedWidget->setCurrentIndex(0);
	showLoadGame();
}

QString mainwindow::formatSeconds(int seconds)
{
	QTime time = QTime(0, 0).addSecs(seconds);

	return time.toString("h:mm:ss");
}

void mainwindow::setUpTabWidges()
{
	ui->twRezultati->setColumnCount(2);
	ui->twRezultati->setColumnWidth(0, 299.5);
	ui->twRezultati->setColumnWidth(1, 299.5);
	ui->twRezultati->setHorizontalHeaderLabels(QStringList() << "Igrac"
															 << "Vreme");

	// for(int i=0; i < ui->twRezultati->rowCount(); i++)
	// {
	//     ime = ui->twRezultati->item(i, 0)->text();
	//     qDebug() << "ime: "<<ime;
	//     ui->twRezultati->removeRow(i);
	// }
	ui->twRezultati->setRowCount(0);

	QVector<Igrac> igraci = rez->getIgraci();
	std::sort(igraci.begin(), igraci.end(),
			  [](const Igrac& obj1, const Igrac& obj2) { return obj1.getVreme() < obj2.getVreme(); });
	qDebug() << igraci.size();
	if (igraci.size() > 0)
	{
		int row = 0;

		for (const Igrac& igrac : igraci)
		{

			if (igrac.getPoslednjiOtkljucaniChallenge() == 16)
			{
				// int row = ui->twRezultati->rowCount();

				ui->twRezultati->insertRow(row);

				QTableWidgetItem* newItemPlayer = new QTableWidgetItem(igrac.getIme());
				newItemPlayer->setFlags(newItemPlayer->flags() ^ Qt::ItemIsEditable);
				ui->twRezultati->setItem(row, 0, newItemPlayer);

				bool ok;
				QString score = formatSeconds(igrac.getVreme());

				QTableWidgetItem* newItemScore = new QTableWidgetItem(score);
				newItemScore->setFlags(newItemScore->flags() ^ Qt::ItemIsEditable);
				ui->twRezultati->setItem(row, 1, newItemScore);

				row++;
			}
		}
	}
	else
	{
		qDebug() << "savefile prazan";
	}
}

void mainwindow::setUpBackground()
{
	QPixmap bkgnd(":/new/resources/resources/cropped_map.png");
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

	QPalette palette;

	palette.setBrush(QPalette::Window, bkgnd);
	this->setPalette(palette);
}

void mainwindow::showLoadGame()
{

	QVector<Igrac> igraci = rez->getIgraci();

	if (igraci.size() > 0)
	{
		ui->pbNastaviIgru->setEnabled(true);
		ui->pbNastaviIgru->setVisible(true);
	}
	else
	{
		ui->pbNastaviIgru->setEnabled(false);
		ui->pbNastaviIgru->setVisible(false);
	}
}

bool mainwindow::eventFilter(QObject* obj, QEvent* event)
{
	if (event->type() == QEvent::MouseButtonPress)
	{
		// Emitujemo signal kada se desi klik
		emit clicked();
		return false;
	}
	return QObject::eventFilter(obj, event);
}

void mainwindow::KlikMelodija()
{
	// Provera da li je došlo do klika na bilo kojem mestu na ekranu
	if (QApplication::activeWindow() != nullptr)
	{
		// Reprodukcija zvuka

		player_klik->setAudioOutput(audioOutput);
		// connect(player, &QMediaPlayer::positionChanged, this,
		// &MediaExample::positionChanged);
		player_klik->setSource(QUrl("qrc:/new/resources/resources/mixkit-arcade-game-jump-coin-216.wav"));
		audioOutput_klik->setVolume(10);
		if (ui->RB_ukljucen->isChecked())
			player_klik->play();
	}
}

void mainwindow::JacinaPozadinseMelodije(int vrednost)
{
	audioOutput->setVolume(static_cast<qreal>(vrednost));
}

void mainwindow::cestitajIgracu()
{
	//    mapa->close();
	PregledRezultataClicked();

	QMessageBox msgBox;
	msgBox.setText("Čestitamo na kompletiranju svih izazova! Kliknite ok da "
				   "biste pregledali svoj rezultat.");

	QAbstractButton* okBtn = msgBox.addButton("Ok", QMessageBox::YesRole);

	msgBox.setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);

	QString buttonStylesheet = "QPushButton { min-width: 100px; min-height: 20px; margin-right: 10px; "
							   "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
							   "padding: 10px; border-radius: 21px; font: bold 14px;}"
							   "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
							   "#bcbcbc; }"
							   "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
							   "#a0a0a0; }";

	QString msgBoxStylesheet = "QMessageBox { font: bold 14px; background-color: rgb(100,100,100);}";

	msgBox.setStyleSheet(msgBoxStylesheet);

	okBtn->setStyleSheet(buttonStylesheet);

	msgBox.exec();

	if (msgBox.clickedButton() == okBtn)
	{
	}
}

void mainwindow::PrikaziUputstvo()
{

	QMessageBox msgBox;
	msgBox.setText("Dobrodošli u BgChallenge! Cilj igrice je da prođeš kroz sve izazove i "
				   "otkriješ što više o Beogradu. Kad dođeš na lokaciju, dobićeš jedan "
				   "izazov. Svaki sadrži nešto karakteristično vezano za tu lokaciju, bilo "
				   "to u vidu šifre koju treba da otkriješ ili samog zadatka. Nakon što ga "
				   "rešiš, prikazaće ti se tekst o toj lokaciji - prilika da saznaš nešto "
				   "novo i zanimljivo o poznatim objektima Beograda. Sledeća lokacija na "
				   "tvojoj turi je misterija: moraćeš da rešiš asocijaciju i da pogodiš gde "
				   "te vodimo dalje! Imaj na umu: odgovor je uglavnom neka turistička "
				   "lokacija. Uživaj u igrici!");
	msgBox.setWindowFlags(Qt::Popup | Qt::FramelessWindowHint);
	msgBox.exec();
}
