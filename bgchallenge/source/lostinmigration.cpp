#include "../include/lostinmigration.h"

#include <QBrush>
#include <QDebug>
#include <QDialog>
#include <QFile>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QUiLoader>
#include <iostream>

using namespace std;

LostInMigration::LostInMigration(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(6, 2, vreme, pocetnoVreme, parent)
{
	this->setWindowTitle("BgChallenge2");
	this->setMinimumWidth(1280);
	this->setMinimumHeight(720);
	this->setFixedSize(this->width(), this->height());

	this->setImagePath(":/new/pozadine/resources/muzej-nikole-tesle.jpg");
	this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

	QFile file(":/new/resources/forms/lostinmig.ui");

	if (!file.open(QFile::ReadOnly))
	{
		qDebug() << "Error opening UI file lostinmig.ui:" << file.errorString();
		return;
	}

	QUiLoader loader;
	ui = loader.load(&file, this);
	ui->setFixedSize(this->width(), this->height());
	file.close();

	Tekstovi();

	QLCDNumber* lcdNumber = ui->findChild<QLCDNumber*>("lcdNumber");

	connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	connect(ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &LostInMigration::zatvori);
	connect(ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &LostInMigration::pomoc);

	// Deklaracija dugmadi
	btnLevo = ui->findChild<QPushButton*>("btnLevo");
	btnDesno = ui->findChild<QPushButton*>("btnDesno");
	btnGore = ui->findChild<QPushButton*>("btnGore");
	btnDole = ui->findChild<QPushButton*>("btnDole");

	btnLevo->setStyleSheet("QPushButton { border-image: "
						   "url(:/new/lostinmigration/resources/"
						   "levostrelica-removebg-preview(1).png); }"
						   "QPushButton:pressed { border-image: "
						   "url(:/new/lostinmigration/resources/"
						   "levostrelica-removebg-preview(1)-pressed.png); }");

	btnDesno->setStyleSheet("QPushButton { border-image: "
							"url(:/new/lostinmigration/resources/"
							"desnostrelica-removebg-preview.png); }"
							"QPushButton:pressed { border-image: "
							"url(:/new/lostinmigration/resources/"
							"desnostrelica-removebg-preview-pressed.png); }");

	btnGore->setStyleSheet("QPushButton { border-image: "
						   "url(:/new/lostinmigration/resources/"
						   "gorestrelica-removebg-preview.png); }"
						   "QPushButton:pressed { border-image: "
						   "url(:/new/lostinmigration/resources/"
						   "gorestrelica-removebg-preview-pressed.png); }");

	btnDole->setStyleSheet("QPushButton { border-image: "
						   "url(:/new/lostinmigration/resources/"
						   "dolestrelica-removebg-preview.png); }"
						   "QPushButton:pressed { border-image: "
						   "url(:/new/lostinmigration/resources/"
						   "dolestrelica-removebg-preview-pressed.png); }");

	leResenjeIzazova = ui->findChild<QLineEdit*>("leResenjeIzazova");
	leResenjeIzazova->setEnabled(false);
	lblUnesiResenje = ui->findChild<QLabel*>("lblUnesiResenje");

	lblDesno1 = ui->findChild<QLabel*>("lblDesno1");
	lblDesno2 = ui->findChild<QLabel*>("lblDesno2");
	lblDole1 = ui->findChild<QLabel*>("lblDole1");
	lblDesno3 = ui->findChild<QLabel*>("lblDesno3");
	lblGore1 = ui->findChild<QLabel*>("lblGore1");
	lblLevo1 = ui->findChild<QLabel*>("lblLevo1");
	lblGore2 = ui->findChild<QLabel*>("lblGore2");
	lblLevo2 = ui->findChild<QLabel*>("lblLevo2");

	leResenjeIzazova->hide();
	lblUnesiResenje->hide();

	resenjeIzazova = "nijagara";

	brojac = 8;
	kombinacija = "";
	ispravnaKombinacija = "desnodesnodoledesnogorelevogorelevo";

	connect(btnLevo, &QPushButton::clicked,
			this, [=]() { kliknuoDugmeLevo(&brojac, &kombinacija, ispravnaKombinacija); });
	connect(btnDesno, &QPushButton::clicked,
			this, [=]() { kliknuoDugmeDesno(&brojac, &kombinacija, ispravnaKombinacija); });
	connect(btnGore, &QPushButton::clicked,
			this, [=]() { kliknuoDugmeGore(&brojac, &kombinacija, ispravnaKombinacija); });
	connect(btnDole, &QPushButton::clicked,
			this, [=]() { kliknuoDugmeDole(&brojac, &kombinacija, ispravnaKombinacija); });
}

LostInMigration::~LostInMigration()
{
	delete ui;
}

void LostInMigration::kliknuoDugmeLevo(int* brojac, QString* kombinacija, QString ispravnaKombinacija)
{

	(*brojac)--;
	kombinacija->append("levo");

	if (*brojac == 0)
	{
		if (kombinacija->compare(ispravnaKombinacija) == 0)
		{
			startFadeDownAnimations();
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setWindowTitle(" ");
			msgBox.setText("Pogrešna kombinacija, probaj ponovo.");
			msgBox.exec();
			*kombinacija = "";
			(*brojac) = 8;
		}
	}
}

void LostInMigration::pomoc()
{
	QDialog dijalog;
	dijalog.setFixedHeight(300);
	dijalog.setFixedWidth(400);
	dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
						  "rgb(200,200,200); margin: 10px;}");

	QLabel* text = new QLabel("Jedan od najsvetlijih umova na svetu, genijalan, očaravajuć, "
							  "i pomalo ekscentričan. Zaljubljenik u nauku,  i u jednu "
							  "golubicu!\nOva jata kriju tajnu kombinaciju. Da li možeš "
							  "pogoditi kombinaciju koja će te dovesti do nje?",
							  &dijalog);
	text->setAlignment(Qt::AlignCenter);
	text->setWordWrap(true);
	text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
	QPushButton* ok = new QPushButton("OK", &dijalog);
	ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
					  "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
					  "padding: 10px; border-radius: 21px; font: bold 14px;}"
					  "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
					  "#bcbcbc; }"
					  "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
					  "#a0a0a0; }");
	QVBoxLayout* layout = new QVBoxLayout(&dijalog);

	layout->addWidget(text);

	layout->addWidget(ok);
	connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
	dijalog.exec();
}

void LostInMigration::kliknuoDugmeGore(int* brojac, QString* kombinacija, QString ispravnaKombinacija)
{
	kombinacija->append("gore");
	(*brojac)--;

	if (*brojac == 0)
	{
		if (kombinacija->compare(ispravnaKombinacija) == 0)
		{
			startFadeDownAnimations();
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setWindowTitle(" ");
			msgBox.setText("Pogrešna kombinacija, probaj ponovo.");
			msgBox.exec();
			*kombinacija = "";
			(*brojac) = 8;
		}
	}
}

void LostInMigration::kliknuoDugmeDesno(int* brojac, QString* kombinacija, QString ispravnaKombinacija)
{
	kombinacija->append("desno");
	(*brojac)--;

	if (*brojac == 0)
	{
		if (kombinacija->compare(ispravnaKombinacija) == 0)
		{
			startFadeDownAnimations();
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setWindowTitle(" ");
			msgBox.setText("Pogrešna kombinacija, probaj ponovo.");
			msgBox.exec();
			*kombinacija = "";
			(*brojac) = 8;
		}
	}
}

void LostInMigration::kliknuoDugmeDole(int* brojac, QString* kombinacija, QString ispravnaKombinacija)
{
	kombinacija->append("dole");
	(*brojac)--;

	if (*brojac == 0)
	{
		if (kombinacija->compare(ispravnaKombinacija) == 0)
		{
			startFadeDownAnimations();
		}
		else
		{
			QMessageBox msgBox;
			msgBox.setWindowTitle(" ");
			msgBox.setText("Pogrešna kombinacija, probaj ponovo.");
			msgBox.exec();
			*kombinacija = "";
			(*brojac) = 8;
		}
	}
}

void LostInMigration::startFadeDownAnimations()
{
	QSequentialAnimationGroup* group = new QSequentialAnimationGroup(this);

	// Vremenski razmak između animacija u milisekundama
	int timeInterval = 50; // Primer: 100 ms

	fadeDownLabel(lblDesno1, group, timeInterval * 0, "N");
	fadeDownLabel(lblDesno2, group, timeInterval * 0.1, "I");
	fadeDownLabel(lblDole1, group, timeInterval * 0.1, "J");
	fadeDownLabel(lblDesno3, group, timeInterval * 0.1, "A");
	fadeDownLabel(lblGore1, group, timeInterval * 0.1, "G");
	fadeDownLabel(lblLevo1, group, timeInterval * 0.1, "A");
	fadeDownLabel(lblGore2, group, timeInterval * 0.1, "R");
	fadeDownLabel(lblLevo2, group, timeInterval * 0.1, "A");
	group->start();
}

void LostInMigration::hideFrame(QLabel* label, QString slovo)
{
	label->hide();
	leResenjeIzazova->insert(slovo);

	btnDesno->hide();
	btnDole->hide();
	btnGore->hide();
	btnLevo->hide();

	leResenjeIzazova->show();
	leResenjeIzazova->setEnabled(true);
	lblUnesiResenje->show();

	connect(leResenjeIzazova, &QLineEdit::returnPressed, this, [=]() {
		foreach (QObject* child, ui->children())
		{
			if (child->isWidgetType())
			{
				QWidget* childWidget = qobject_cast<QWidget*>(child);
				if (childWidget)
				{
					qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

					childWidget->hide();
				}
			}
		}

		foreach (QObject* child, this->children())
		{
			if (child->isWidgetType())
			{
				QWidget* childWidget = qobject_cast<QWidget*>(child);
				if (childWidget)
				{
					qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

					childWidget->hide();
				}
			}
		}

		QLayout* postojeciLayout = this->layout();
		if (postojeciLayout)
		{
			delete postojeciLayout;
		}

		prikaziEdukativniTekst();
	});
}

void LostInMigration::fadeDownLabel(QLabel* label, QSequentialAnimationGroup* group, int startDelay, QString slovo)
{

	QPropertyAnimation* animation = new QPropertyAnimation(label, "geometry");

	// Postavite trajanje animacije u milisekundama
	animation->setDuration(700); // Primer: 1000 ms = 1 sekunda

	// Postavite krajnje vrednosti za animaciju (u ovom slučaju, pomerajte QLabel
	// prema dole)
	animation->setStartValue(label->geometry());
	animation->setEndValue(label->geometry().translated(0, label->height()));

	// Postavite kašnjenje pre nego što animacija počne
	// animation->setStartValue(startDelay);

	connect(animation, &QPropertyAnimation::finished, this, [=]() { hideFrame(label, slovo); });

	// leResenjeIzazova->insert(slovo);

	// Dodajte animaciju u grupu
	group->addAnimation(animation);
}

void LostInMigration::Tekstovi()
{

	m_edukativniTekst = R"(
Ukoliko šetate Krunskom ulicom, teško da će vam promaći impresivna vila sa zanimljivim stepenicama i lučnim ulazom. U pitanju je porodična kuća Đorđa Genčića, za koga se smatra da je odgovaran za osmišljanje Majskog prevrata i ubistvo Aleksandra Obrenovića i Drage Mašin, a u kojoj je danas smešten Muzej Nikole Tesle.

Da li ste znali da se tu nalazi jedan njujorški krevet? I to ne običan, već krevet iz sobe 3327 hotela Njujorker, u kojoj je Nikola Tesla proveo deset poslednjih godina života. Nikola Tesla umro je u Njujorku 1943. godine. Nikad se nije ženio niti imao decu pa je njegov najbliži srodnik, njegov sestrić Sava Kosanović sve njegove lične stvari doneo u Beograd i zaveštao za osnivanje Muzeja Nikole Tesle. Urna sa pepelom Nikole Tesle doneta je u muzej 1957. godine, čime on postaje i memorijalni muzej.
Na policama u podrumu Genčićeve vile je i frižider iz Tesline hotelske sobe, ali i obilje dokumenata, Teslinih ličnih stvari i garderobe: pribor za brijanje, naočare, sanduk u kome je dopremljena zaostavština, svilene košulje, odela, kravate i rukavice.

Nikola Tesla je u Beogradu bio samo jednom, 1892. godine, na tri dana.Tada još uvek mladi naučnik Nikola Tesla, prihvatio je poziv rektora Beogradskog univerziteta Đorđa Stanojevića i 1. juna predveče stigao na beogradsku Železničku stanicu. Dočekali su ga mnogi ugledni Beograđani, ali i masa “običnog” sveta kod kog je taj “naučnik svetskog glasa” izazvao radoznalost i znatiželju. U ta 3 dana njegov je raspored bio gust.
Poseta je uključivala upoznavanje sa mladim kraljem, posetu Narodnom muzeju, Velikoj školi, šetnju Kalemegdanom, ali i svečanu večeru u njegovu čast. Tom prilikom poznati srpski pesnik Jovan Jovanović Zmaj pročitao je pesmu koju je napisao i posvetio Tesli.

Tesla je bio duboko ganut tim činom i kada se vratio u Njujork jedan od svojih transformatora posvetio je Zmaju. To je bio Teslin način da mu se oduži i ukaže poštovanje.
Sam uređaj prate sijalice koje ispisuju nadimak poznatog pesnika. )";

	m_asocijacija = "Religija";

	m_hint = "Nalazi se nadomak Karađorđevog parka. Veliki je. Baš je poznat po "
			 "tome što je veliki.";

	m_resenjeAsocijacije.append("hram svetog save");
	m_resenjeAsocijacije.append("hram");
	m_resenjeAsocijacije.append("hram sv save");
	m_naslovEdukativnogTeksta = "O Muzeju Nikole Tesle";
}
