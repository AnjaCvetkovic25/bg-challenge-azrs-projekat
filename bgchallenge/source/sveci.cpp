#include "../include/sveci.h"
#include "../forms/ui_sveci.h"
#include <QMessageBox>
#include <QPainter>
#include <QTime>

Sveci::Sveci(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(11, 3, vreme, pocetnoVreme, parent), ui(new Ui::Sveci)
{
	ui->setupUi(this);

	// pozadina
	this->setImagePath(":/new/sveci/resources/pozadinaa.png");

	// connect(&m_timer, &QTimer::timeout, this, &Sveci::timerExpired);

	/*QPixmap bkgnd(":/new/sveci/resources/pozadinaa.png");
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio,
	Qt::SmoothTransformation); QPalette palette;
	palette.setBrush(QPalette::Window, bkgnd);
	this->setPalette(palette);*/
	Tekstovi();

	connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		ui->lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});

	// Kreiranje dugmadi
	QString skriveniBtnStylesheet = "QPushButton { background-color: rgba(0, 0, 0, 0.5); color: white; "
									"border: 1px solid white; border-radius: 5px; padding: 5px 10px; "
									"min-width: 100px; max-width:100px; min-height: 100px; max-height:100px;}"
									"QPushButton:hover { background-color: rgba(255, 255, 255, 0.2);}"
									"QPushButton:pressed { background-color: rgba(255, 255, 255, 0.4);}";

	button1 = new QPushButton("F", this);
	button1->setVisible(false);
	button1->setStyleSheet(skriveniBtnStylesheet);

	button2 = new QPushButton("R", this);
	button2->setVisible(false);
	button2->setStyleSheet(skriveniBtnStylesheet);

	button3 = new QPushButton("E", this);
	button3->setVisible(false);
	button3->setStyleSheet(skriveniBtnStylesheet);

	button4 = new QPushButton("S", this);
	button4->setVisible(false);
	button4->setStyleSheet(skriveniBtnStylesheet);

	button5 = new QPushButton("K", this);
	button5->setVisible(false);
	button5->setStyleSheet(skriveniBtnStylesheet);

	button6 = new QPushButton("A", this);
	button6->setVisible(false);
	button6->setStyleSheet(skriveniBtnStylesheet);

	labela = new QLabel();
	labela->setText("");

	labela->setFont(QFont("ARIAL", 70));
	labela->setStyleSheet("border: 2px solid black;");
	labela->setStyleSheet("text-decoration: underline;");
	labela->setAlignment(Qt::AlignCenter);
	ui->horizontalLayout_2->addWidget(labela);
	ui->horizontalLayout_2->setAlignment(Qt::AlignCenter);

	connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(proveri()));
	connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(proveri2()));
	connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(proveri3()));
	connect(ui->pushButton_4, SIGNAL(clicked()), this, SLOT(proveri4()));
	connect(ui->pushButton_5, SIGNAL(clicked()), this, SLOT(proveri5()));
	connect(ui->pushButton_6, SIGNAL(clicked()), this, SLOT(proveri6()));

	connect(button1, SIGNAL(clicked()), this, SLOT(F()));
	connect(button2, SIGNAL(clicked()), this, SLOT(R()));
	connect(button3, SIGNAL(clicked()), this, SLOT(E()));
	connect(button4, SIGNAL(clicked()), this, SLOT(S()));
	connect(button5, SIGNAL(clicked()), this, SLOT(K()));
	connect(button6, SIGNAL(clicked()), this, SLOT(A()));

	connect(ui->btnNazadNaLokaciju, &QPushButton::clicked, this, &Sveci::zatvori);
}

Sveci::~Sveci()
{
	delete ui;
}

void Sveci::proveri()
{
	if (ui->lineEdit->text().compare("SAVA", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit->text().compare("SV SAVA", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit->text().compare("SVETI SAVA", Qt::CaseInsensitive) == 0)
	{
		// QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

		// Dodavanje dugmadi u horizontalni raspored
		ui->horizontalLayout->addWidget(button5);
		button5->setVisible(true);
	}
	else
	{
		// QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
		ui->lineEdit->clear();
	}
}

void Sveci::proveri2()
{
	if (ui->lineEdit_2->text().compare("JOVAN", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_2->text().compare("SV JOVAN", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_2->text().compare("SVETI JOVAN", Qt::CaseInsensitive) == 0)
	{
		// QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

		// Dodavanje dugmadi u horizontalni raspored
		ui->horizontalLayout->addWidget(button4);
		button4->setVisible(true);
	}
	else
	{
		// QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
		ui->lineEdit_2->clear();
	}
}
void Sveci::proveri3()
{
	if (ui->lineEdit_3->text().compare("PETKA", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_3->text().compare("PARASKEVA", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_3->text().compare("SV PETKA", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_3->text().compare("SVETA PETKA", Qt::CaseInsensitive) == 0)
	{
		// QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

		// Dodavanje dugmadi u horizontalni raspored
		ui->horizontalLayout->addWidget(button6);
		button6->setVisible(true);
	}
	else
	{
		QMessageBox::warning(this, "Greska", "Molim vas pokusajte opet!!!");
		ui->lineEdit_3->clear();
	}
}
void Sveci::proveri4()
{
	if (ui->lineEdit_4->text().compare("VASILIJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_4->text().compare("SV VASILIJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_4->text().compare("SVETI VASILIJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_4->text().compare("VASILIJE OSTROŠKI", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_4->text().compare("SV VASILIJE OSTROŠKI", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_4->text().compare("SVETI VASILIJE OSTROŠKI", Qt::CaseInsensitive) == 0)
	{
		// QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

		// Dodavanje dugmadi u horizontalni raspored
		ui->horizontalLayout->addWidget(button3);
		button3->setVisible(true);
	}
	else
	{
		// QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
		ui->lineEdit_4->clear();
	}
}
void Sveci::proveri5()
{
	if (ui->lineEdit_5->text().compare("GEORGIJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("SV GEORGIJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("SVETI GEORGIJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("ĐORĐE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("SV ĐORĐE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("SVETI ĐORĐE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("DJORDJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("SV DJORDJE", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_5->text().compare("SVETI DJORDJE", Qt::CaseInsensitive) == 0)
	{
		// QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

		// Dodavanje dugmadi u horizontalni raspored
		ui->horizontalLayout->addWidget(button2);
		button2->setVisible(true);
	}
	else
	{
		// QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
		ui->lineEdit_5->clear();
	}
}
void Sveci::proveri6()
{
	if (ui->lineEdit_6->text().compare("BELI ANDJEO", Qt::CaseInsensitive) == 0 ||
		ui->lineEdit_6->text().compare("BELI ANĐEO", Qt::CaseInsensitive) == 0)
	{
		// QMessageBox::information(this,"Bravo", "Bravo pogodili ste");

		// Dodavanje dugmadi u horizontalni raspored
		ui->horizontalLayout->addWidget(button1);
		button1->setVisible(true);
	}
	else
	{
		// QMessageBox::warning(this,"Greska", "Molim vas pokusajte opet!!!");
		ui->lineEdit_6->clear();
	}
}

void Sveci::F()
{
	// QLabel *labela = new QLabel();
	labela->setText(labela->text() + "F");
	// ui->horizontalLayout_2->addWidget(labela);
	button1->setEnabled(false);

	if (labela->text() == "FRESKA")
	{
		QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
		m_timer.stop();
		// ui->setVisible(false);

		QObjectList children = this->children();
		for (QObject* child : children)
		{
			if (QWidget* widget = qobject_cast<QWidget*>(child))
			{
				widget->setVisible(false);
			}
		}
		QLayout* postojeciLayout = this->layout();
		if (postojeciLayout)
		{
			delete postojeciLayout;
		}
		this->prikaziEdukativniTekst();
		// zavrsiChallenge();
	}
	else if (labela->text().length() == 6)
	{
		QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
		labela->setText("");
		button1->setEnabled(true);
		button2->setEnabled(true);
		button3->setEnabled(true);
		button4->setEnabled(true);
		button5->setEnabled(true);
		button6->setEnabled(true);
	}
}
void Sveci::R()
{
	// QLabel *labela = new QLabel();
	labela->setText(labela->text() + "R");
	// ui->horizontalLayout_2->addWidget(labela);
	button2->setEnabled(false);

	if (labela->text() == "FRESKA")
	{
		QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
		zavrsiChallenge();
	}
	else if (labela->text().length() == 6)
	{
		QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
		labela->setText("");
		button1->setEnabled(true);
		button2->setEnabled(true);
		button3->setEnabled(true);
		button4->setEnabled(true);
		button5->setEnabled(true);
		button6->setEnabled(true);
	}
}

void Sveci::E()
{
	// QLabel *labela = new QLabel();
	labela->setText(labela->text() + "E");
	// ui->horizontalLayout_2->addWidget(labela);
	button3->setEnabled(false);

	if (labela->text() == "FRESKA")
	{
		QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
		zavrsiChallenge();
	}
	else if (labela->text().length() == 6)
	{
		QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
		labela->setText("");
		button1->setEnabled(true);
		button2->setEnabled(true);
		button3->setEnabled(true);
		button4->setEnabled(true);
		button5->setEnabled(true);
		button6->setEnabled(true);
	}
}
void Sveci::S()
{
	// QLabel *labela = new QLabel();
	labela->setText(labela->text() + "S");
	// ui->horizontalLayout_2->addWidget(labela);
	button4->setEnabled(false);

	if (labela->text() == "FRESKA")
	{
		QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
		zavrsiChallenge();
	}
	else if (labela->text().length() == 6)
	{
		QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
		labela->setText("");
		button1->setEnabled(true);
		button2->setEnabled(true);
		button3->setEnabled(true);
		button4->setEnabled(true);
		button5->setEnabled(true);
		button6->setEnabled(true);
	}
}
void Sveci::K()
{
	// QLabel *labela = new QLabel();
	labela->setText(labela->text() + "K");
	// ui->horizontalLayout_2->addWidget(labela);
	button5->setEnabled(false);

	if (labela->text() == "FRESKA")
	{
		QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
		zavrsiChallenge();
	}
	else if (labela->text().length() == 6)
	{
		QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
		labela->setText("");
		button1->setEnabled(true);
		button2->setEnabled(true);
		button3->setEnabled(true);
		button4->setEnabled(true);
		button5->setEnabled(true);
		button6->setEnabled(true);
	}
}
void Sveci::A()
{
	// QLabel *labela = new QLabel();
	labela->setText(labela->text() + "A");
	// ui->horizontalLayout_2->addWidget(labela);
	button6->setEnabled(false);

	if (labela->text() == "FRESKA")
	{
		QMessageBox::information(this, "BRAVO", "POGODILI STE REC");
		ui->btnNazadNaLokaciju->setVisible(false);
		ui->pushButton->setVisible(false);
		ui->pushButton_2->setVisible(false);
		ui->pushButton_3->setVisible(false);
		ui->pushButton_4->setVisible(false);
		ui->pushButton_5->setVisible(false);
		ui->pushButton_6->setVisible(false);

		ui->btnNazadNaLokaciju->setVisible(false);
		ui->label->setVisible(false);
		ui->label_2->setVisible(false);
		ui->label_3->setVisible(false);
		ui->label_4->setVisible(false);
		ui->label_5->setVisible(false);
		ui->label_6->setVisible(false);
		ui->label_7->setVisible(false);
		ui->label_8->setVisible(false);
		ui->label_9->setVisible(false);
		ui->label_10->setVisible(false);
		ui->label_11->setVisible(false);
		ui->label_12->setVisible(false);
		labela->setVisible(false);

		ui->lineEdit->setVisible(false);
		ui->lineEdit_2->setVisible(false);
		ui->lineEdit_3->setVisible(false);
		ui->lineEdit_4->setVisible(false);
		ui->lineEdit_5->setVisible(false);
		ui->lineEdit_6->setVisible(false);

		button1->setVisible(false);
		button2->setVisible(false);
		button3->setVisible(false);
		button4->setVisible(false);
		button5->setVisible(false);
		button6->setVisible(false);
		m_timer.stop();
		// ui->setVisible(false);

		QObjectList children = this->children();
		for (QObject* child : children)
		{
			if (QWidget* widget = qobject_cast<QWidget*>(child))
			{
				widget->setVisible(false);
			}
		}
		QLayout* postojeciLayout = this->layout();
		if (postojeciLayout)
		{
			delete postojeciLayout;
		}

		prikaziEdukativniTekst();
		zavrsiChallenge();
	}
	else if (labela->text().length() == 6)
	{
		QMessageBox::warning(this, "Greska", "Probajte ponovo niste pogodili");
		labela->setText("");
		button1->setEnabled(true);
		button2->setEnabled(true);
		button3->setEnabled(true);
		button4->setEnabled(true);
		button5->setEnabled(true);
		button6->setEnabled(true);
	}
}

void Sveci::Tekstovi()
{

	m_edukativniTekst = R"(
Crkva Svetog Marka je izgrađena između 1931. i 1940. godine, ali je po izbijanju Drugog Svetskog rata prekinuta izgradnja. Tada u tom periodu su izvršeni samo građevinski radovi, ali su uprkos tome bogosluženja bila vršena i tokom i posle rata, sve do 14. novembra 1948.

Izgrađena je u srpsko-vizantijskom stilu, po modelu manastira Gračanica. Spoljna fasada urađena je polihromno, tačnije u više boja, a sama veličina zadnja je impozantna i veličanstvena. Iznad ulaznih vrata na spoljnoj fasadi nalazi se mozaička ikona Svetog Apostola i Evangelista Marka, iz 1961. godine, delo Veljka Stojanovića. Prostire se preko 1150 kvadratnih metara, a njen naos može da ugosti i do 2000 vernika, a horska galerija preko 150 pevača.

Ikonostas je rađen po projektu Zorana Petrovića, a napravljen je od mermera dok su ikone koje ga krase rađene tehnikom mozaika i delo su Đure Radlovića, akademskog slikara. Pored ikonostasa u mermeru rađena je i časna trpeza, a ukrašena je manjim mozaicima na prednjoj strani.

Sa desne strane glavnog oltara nalazi se znatno manji oltar Svetom despotu Stefanu Lazareviću, dok se sa leve strane nalazi oltar posvećen prazniku Preobraženja Gospodnjeg. Grobnica cara Stefana Dušana se nalazi uz južni zid, rađena po projektu Dragomira Tadića, u mermeru, gde su položene mošti cara Stefana Dušana donesene iz manastira Svetih Arhandjela. Nasuprot grobnice cara Stefana Dušana izgrađena je grobnica patrijarha Germana.

Na sredini se nalazi bakarni polijelej, rađen po projektu Dragomira Tadića, a napravio ga je Dragutin Petrović.

Kripta se nalazi ispod crkve, a urađena je 2007. godine, u njoj se nalaze mošti Mitropolita Teodosija, Episkopa niškog Viktora, Episkopa šabačkog Gavrila i Episkopa timočkog Mojsija, takođe se nalaze i posmrtni ostaci Kralja Aleksandra Obrenovića i Kraljice Drage, Ane Jovane Obrenović, Knjaza Milana Obrenovića i Kneževića Sergija Obrenovića, kao i grob ktitora starog hrama Lazara Pančea)";
	m_asocijacija = "Tamo se održavaju dobri koncerti (na otvorenom)";

	m_resenjeAsocijacije.append("tasmajdan");
	m_resenjeAsocijacije.append("tašmajdan");

	m_hint = "Možeš i da treniraš, i da plivaš na bazenima, i da šetaš kučiće, i "
			 "da ideš na koncerte, sve to pored Bulevara ";
	m_naslovEdukativnogTeksta = "O Crkvi Svetog Marka";
}
