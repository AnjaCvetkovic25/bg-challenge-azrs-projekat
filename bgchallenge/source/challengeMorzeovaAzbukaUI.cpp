#include "../include/challengeMorzeovaAzbukaUI.h"

#include <QBrush>
#include <QDebug>
#include <QFile>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPushButton>
#include <QUiLoader>
ChallengeMorzeovaAzbukaUI::ChallengeMorzeovaAzbukaUI(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(3, 1, vreme, pocetnoVreme, parent)
{

	this->setWindowTitle("BgChallenge");
	this->setMinimumWidth(1280);
	this->setMinimumHeight(720);
	this->setFixedSize(this->width(), this->height());

	this->setImagePath(":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_ugaseno.png");
	this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

	Tekstovi();

	connect(&m_timer, &QTimer::timeout, this, &ChallengeMorzeovaAzbukaUI::timerExpired);

	// ucitavanje ui
	QFile file(":/new/morzeovaAbuka/forms/morzeovaAzbuka.ui");

	if (!file.open(QFile::ReadOnly))
	{
		qDebug() << "Error opening UI file:" << file.errorString();
		return;
	}

	QUiLoader loader;
	ui = loader.load(&file, this);
	ui->setFixedSize(this->width(), this->height());

	file.close();

	QLCDNumber* lcdNumber = ui->findChild<QLCDNumber*>("lcdNumber");
	QPushButton* pogodiBtn = ui->findChild<QPushButton*>("pbPogodi");
	QLineEdit* lineEdit = ui->findChild<QLineEdit*>("lineEdit");
	QPushButton* ponoviBtn = ui->findChild<QPushButton*>("pbPonovi");
	connect(ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &ChallengeMorzeovaAzbukaUI::zatvori);

	connect(pogodiBtn, &QPushButton::clicked, this, [=]() {
		QString unetaRec = lineEdit->text();
		lineEdit->clear();
		pogodi(unetaRec);
	});

	connect(ponoviBtn, &QPushButton::clicked, this, &ChallengeMorzeovaAzbukaUI::spelujRec);

	connect(m_vreme, &Timer::azurirajVreme, this, [=](int vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});
}

ChallengeMorzeovaAzbukaUI::~ChallengeMorzeovaAzbukaUI()
{
	delete ui;
}

void ChallengeMorzeovaAzbukaUI::pogodi(const QString& r)
{

	if (r.toLower() == m_rec.toLower())
	{
		m_timer.stop();
		ui->setVisible(false);
		QObjectList children = this->children();
		for (QObject* child : children)
		{
			if (QWidget* widget = qobject_cast<QWidget*>(child))
			{
				widget->setVisible(false);
			}
		}
		QLayout* postojeciLayout = this->layout();
		if (postojeciLayout)
		{
			delete postojeciLayout;
		}
		prikaziEdukativniTekst();
	}
}

void ChallengeMorzeovaAzbukaUI::spelujRec()
{
	m_currentIndex = 0;
	m_timer.start(300);
}

void ChallengeMorzeovaAzbukaUI::timerExpired()
{
	if (m_currentIndex < m_kod.length())
	{
		QChar currentCharacter = m_kod.at(m_currentIndex);
		if (currentCharacter == '.')
		{

			setImagePath(":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_upaljeno.png");

			// currentIndex++;
			m_timer.start(300);
		}

		else if (currentCharacter == '-')
		{
			setImagePath(":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_upaljeno.png");
			// currentIndex++;
			m_timer.start(900);
		}
		else if (currentCharacter == ' ')
		{
			setImagePath(":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_ugaseno.png");
			// currentIndex++;
			m_timer.start(300);
		}
		else if (currentCharacter == '#')
		{
			setImagePath(":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_ugaseno.png");
			// currentIndex++;
			m_timer.start(600);
		}
	}
	else
	{
		setImagePath(":/new/morzeovaAbuka/resources/morzeovaAzbukaBg_ugaseno.png");

		m_timer.stop();
	}

	m_currentIndex++;
}

void ChallengeMorzeovaAzbukaUI::Tekstovi()
{

	m_edukativniTekst =
		R"(Na svom samom početku (koji se gradio više od 20 godina), to je bio hotel de luks kategorije, simbol uspešnog razvoja Jugoslavije. Upravo na bazenu ovog hotela,
balkanski špijun Ilija Čvorović juri svog podstanara iz više klase jer je taj bazen postao mesto okupljanja džet-seta u komunizmu.

Hotel je pogođen u NATO bombardovanju 1999. I od tada je zatvoren.

Bio je to zapadnjački hotel s istočnjačkim cenama. Prenoćište sa doručkom je koštalo od 45 do 75 dolara, plus 0.60 dolara koliko je trebalo izdvojiti za boravišnu taksu.

Osim bazena, u Jugoslaviji je bilo I drugih prilika za uživanje.

I tada, u hotelu je ordinirala grupa stalnih prostitutki. Sedele su za barom I čekale da im klijenti naruče piće. Potom su s njima odlazile u sobe. Ovlašćeno osoblje je žmurilo na ovaj prekršaj –
imali su I oni udela u slatkom poslu. Bila je to javna tajna kojoj se gledalo kroz prste. Premda je prostitucija bila, I sada je, zakonom zabranjena, u hotelu koji je bio simbol prestiža I ponosa
mislili su da taj zakon ne važi. Međutim...

Jedne ni po čemu neobične noći u hotel su upali policajci u civilu. Uhapšeno je nekoliko prostituki i recepcioneri. Klijenti nisu privođeni - korišćenje usluga nije bilo
kažnjivo, ali se podrazumevalo da novac koji su dali neće biti vraćen.
Novac zatečen kod osobe koja se bavila podvođenjem plenio se u korist države, odnosno uplaćivao ustanovama za socijalno staranje. Posle izvesnog vremena, *posao* je nastavljen, ali oprezno, sa izvesnom
dozom straha.

Hotel Jugoslavija je napušten već više od dvadeset godina. Niti se prodaje, niti se ruši. Samo postoji, kao svedok vremena koje je prošlo.)";

	// TODO treba da dodam pasus o tome kako su italijani rekli da su nam dali kao
	// dali kristal, a ispostavilo se da je to obicno staklo

	m_asocijacija = "Tamo preko granice (u Zemunu)"; // zamisao je da ovde stoji slika kule od
													 // peska, dodala sam je u resources.qrc
													 // ://resources/kula.jpg

	m_resenjeAsocijacije.append("gardos kula");
	m_resenjeAsocijacije.append("gardos");
	m_resenjeAsocijacije.append("gardoš");
	m_resenjeAsocijacije.append("gardoš kula");

	m_hint = "Uzdiže se na kraju Zemunskog keja.";

	m_naslovEdukativnogTeksta = "O Hotelu Jugoslavija";
}

QString ChallengeMorzeovaAzbukaUI::getRec()
{
	return m_rec;
}
QString ChallengeMorzeovaAzbukaUI::getKod()
{
	return m_kod;
}

QTimer& ChallengeMorzeovaAzbukaUI::getTimer()
{
	return m_timer;
}

int ChallengeMorzeovaAzbukaUI::getCurrentIndex()
{
	return m_currentIndex;
}

QString ChallengeMorzeovaAzbukaUI::getImagePath()
{
	return m_imagePath;
}
