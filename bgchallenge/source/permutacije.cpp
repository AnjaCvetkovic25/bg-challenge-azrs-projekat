#include "../include/permutacije.h"

#include <QDebug>
#include <QPainter>

#include <QBrush>
#include <QDialog>
#include <QFile>
#include <QFontDatabase>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QPalette>
#include <QPixmap>
#include <QPushButton>
#include <QString>
#include <QTextBrowser>
#include <QUiLoader>

Permutacije::Permutacije(Timer* vreme, int pocetnoVreme, QWidget* parent)
	: Challenge_test(14, 4, vreme, pocetnoVreme, parent)
{
	// qDebug() << "usao je u konstruktor";
	this->setWindowTitle("BgChallenge2");
	this->setMinimumWidth(1280);
	this->setMinimumHeight(720);
	this->setFixedSize(this->width(), this->height());

	this->setImagePath(":/new/pozadine/resources/trg(1).jpg");
	this->setWindowIcon(QIcon(":/new/resources/resources/icon.png"));

	QFile file(":/new/resources/forms/permutacije.ui");

	if (!file.open(QFile::ReadOnly))
	{
		qDebug() << "Error opening UI file permutacije.ui:" << file.errorString();
		return;
	}

	QUiLoader loader;
	ui = loader.load(&file, this);
	ui->setFixedSize(this->width(), this->height());
	file.close();

	QLCDNumber* lcdNumber = ui->findChild<QLCDNumber*>("lcdNumber");

	connect(m_vreme, &Timer::azurirajVreme, this, [=](qint64 vreme) {
		m_pocetnoVreme++;
		QTime time = QTime(0, 0).addSecs(m_pocetnoVreme);
		lcdNumber->display(time.toString("h:mm:ss"));
		update();
	});
	connect(ui->findChild<QPushButton*>("pbNazad"), &QPushButton::clicked, this, &Permutacije::zatvori);
	connect(ui->findChild<QPushButton*>("pbPomoc"), &QPushButton::clicked, this, &Permutacije::pomoc);

	Tekstovi();

	// Deklaracija dugmadi
	lblPocetna = ui->findChild<QLabel*>("lblPocetna");
	lblStambol = ui->findChild<QLabel*>("lblStambol");

	tbUputstvo = ui->findChild<QTextBrowser*>("tbUputstvo");

	btnTrougao = ui->findChild<QPushButton*>("btnTrougao");
	btnZvezda = ui->findChild<QPushButton*>("btnZvezda");
	btnKrug = ui->findChild<QPushButton*>("btnKrug");
	btnReset = ui->findChild<QPushButton*>("btnReset");

	btnTrougao->setStyleSheet("QPushButton { border-image: "
							  "url(:/new/permutacije/resources/trougao-clicked.png); }"
							  "QPushButton:pressed { border-image: "
							  "url(:/new/permutacije/resources/trougao(1).png); }");

	btnZvezda->setStyleSheet("QPushButton { border-image: "
							 "url(:/new/permutacije/resources/krug-clicked.png); }"
							 "QPushButton:pressed { border-image: "
							 "url(:/new/permutacije/resources/krug(1).png); }");

	btnKrug->setStyleSheet("QPushButton { border-image: "
						   "url(:/new/permutacije/resources/zvezda-clicked.png); }"
						   "QPushButton:pressed { border-image: "
						   "url(:/new/permutacije/resources/zvezda.png); }");

	btnReset->setStyleSheet("border-image: url(:/resources/hanojska_kula/retry.png)");

	kombinacija = lblPocetna->text();
	resenje = lblStambol->text();

	connect(btnTrougao, SIGNAL(clicked()), this, SLOT(kliknuoTrougao()));
	connect(btnZvezda, SIGNAL(clicked()), this, SLOT(kliknuoZvezdu()));
	connect(btnKrug, SIGNAL(clicked()), this, SLOT(kliknuoKrug()));

	connect(btnReset, SIGNAL(clicked()), this, SLOT(kliknuoReset()));

	prethodniKlik = "";

	// ovo je bila glupa ideja, treba da promenim modifikacije

	QFontDatabase::addApplicationFont(":/new/permutacije/resources/OldNewspaperTypes.ttf");
	int fontId = QFontDatabase::addApplicationFont(":/new/permutacije/resources/OldNewspaperTypes.ttf");

	if (fontId != -1)
	{
		QString customFontFamily = QFontDatabase::applicationFontFamilies(fontId).at(0);
		lblPocetna->setStyleSheet("font-family: '" + customFontFamily + "'; font-size: 35px;");
		lblStambol->setStyleSheet("font-family: '" + customFontFamily + "'; font-size: 35px;");
	}
	else
	{
		qDebug() << "Error loading font";
	}
}

Permutacije::~Permutacije()
{
	delete ui;
}

void Permutacije::Tekstovi()
{

	m_edukativniTekst = R"(
Sadašnji trg formiran je posle rušenja Stambol-kapije i podizanja zgrade Narodnog pozorišta.
Stambol-kapija, koju su sagradili Austrijanci početkom 18. veka, nalazila se između spomenika knezu Mihailu i Narodnog pozorišta. To je bila najveća i najlepša kapija u vreme dok je grad bio opisan šansom. Kroz nju je vodio put za Carigrad po kojem je i dobila ovaj naziv. U narodu je Stambol-kapija ostala upamćena po tome što su Turci na prostoru ispred nje vršili egzekuciju "sirotinje raje" nabijanjem na kolac. Prilikom zauzimanja Beograda (1806) pred ovom kapijom smrtno je ranjen Vasa Čarapić, poznati vojskovođa iz Prvog srpskog ustanka. U spomen na ovaj događaj ulica u blizini ovog mesta dobila je njegovo ime, a podignut je i spomenik. Nakon uspostavljanja srpske vlasti i rušenja Stambol-kapije, prostor današnjeg Trga Republike dugo je ostao neizgrađen. Narodno pozorište stajalo je više od 30 godina kao jedina velika zgrada.

Kada je 1882. podignut spomenik knezu Mihailu, počelo je postepeno urbano formiranje ovog trga. Na mestu današnjeg Narodnog muzeja bila je podignuta dugačka prizemna zgrada u kojoj se, između ostalog, nalazila poznata kafana "Dardaneli", stecište umetničkog sveta. Zgrada je srušena da bi se na njenom mestu 1903. podigla Uprava fondova.

Centrom Beograda prolazile su tramvajske šine i prva tramvajska linija broj 1, koja je saobraćala od trga Slavije do Kalemegdana. Knez Mihailova ulica dugo nije važila za pešačku zonu i bila je otvorena za svakodnevni saobraćaj.

U malom parku pored Narodnog pozorišta, sve do Drugog svetskog rata, nalazili su se poznata kafana i bioskop "Kolarac". Palata "Riunione", u kojoj se nalazi bioskop "Jadran", sagrađena je 1930.)";
	m_asocijacija = "Mesto za bleju MATFovaca kada je prolećni semestar";
	m_hint = "Odavde se vidi Beograd sa suprotne strane Branka";
	m_resenjeAsocijacije.append("kalemegdan");
	m_naslovEdukativnogTeksta = "O Trgu Republike";
}

void Permutacije::kliknuoTrougao()
{

	if (prethodniKlik != "Trougao")
	{
		QString novaKombinacija = "";

		// trougao je -3
		for (QChar slovo : qAsConst(kombinacija))
		{
			int brojSlova = slovo.unicode() - 64;
			// qDebug() << "slovo " << slovo << " je broj " << broj_slova;

			int noviBrojSlova = ((brojSlova - 2) + 26) % 26;
			QChar novoSlovo = QChar::fromLatin1('A' + noviBrojSlova);

			novaKombinacija.append(novoSlovo);
		}

		kombinacija = novaKombinacija;
		lblPocetna->setText(kombinacija);

		PoklopljenaSlova();

		// qDebug() << nova_kombinacija;
		prethodniKlik = "Trougao";
	}
}

void Permutacije::kliknuoZvezdu()
{

	if (prethodniKlik != "Zvezda")
	{
		QString novaKombinacija = "";

		// zvezda je +2
		for (QChar slovo : qAsConst(kombinacija))
		{
			int brojSlova = slovo.unicode() - 64;
			// Debug() << "slovo " << slovo << " je broj " << broj_slova;

			int noviBrojSlova = ((brojSlova + 3) + 26) % 26;
			QChar novoSlovo = QChar::fromLatin1('A' + noviBrojSlova);

			novaKombinacija.append(novoSlovo);
		}

		kombinacija = novaKombinacija;
		lblPocetna->setText(kombinacija);

		PoklopljenaSlova();

		// qDebug() << nova_kombinacija;
		prethodniKlik = "Zvezda";
	}
}

void Permutacije::kliknuoKrug()
{

	if (prethodniKlik != "Krug")
	{
		QString novaKombinacija = "";

		// krug je +5
		for (QChar slovo : qAsConst(kombinacija))
		{
			int brojSlova = slovo.unicode() - 64;
			// qDebug() << "slovo " << slovo << " je broj " << broj_slova;

			int noviBrojSlova = ((brojSlova + 1) + 26) % 26;
			QChar novoSlovo = QChar::fromLatin1('A' + noviBrojSlova);

			novaKombinacija.append(novoSlovo);
		}

		kombinacija = novaKombinacija;
		lblPocetna->setText(kombinacija);

		PoklopljenaSlova();

		// qDebug() << nova_kombinacija;
		prethodniKlik = "Krug";
	}
}

void Permutacije::kliknuoReset()
{
	kombinacija = "KLSETGD";
	lblPocetna->setText(kombinacija);

	connect(btnTrougao, SIGNAL(clicked()), this, SLOT(kliknuoTrougao()));
	connect(btnZvezda, SIGNAL(clicked()), this, SLOT(kliknuoZvezdu()));
	connect(btnKrug, SIGNAL(clicked()), this, SLOT(kliknuoKrug()));
}

void Permutacije::PoklopljenaSlova()
{
	if (kombinacija == resenje)
	{
		// qDebug() << "Poklopljena slova!";
		disconnect(btnTrougao, SIGNAL(clicked()), this, SLOT(kliknuoTrougao()));
		disconnect(btnZvezda, SIGNAL(clicked()), this, SLOT(kliknuoZvezdu()));
		disconnect(btnKrug, SIGNAL(clicked()), this, SLOT(kliknuoKrug()));

		// Postavi QTimer za odgodu od 2 sekunde
		QTimer::singleShot(1000, this, [=]() {
			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			foreach (QObject* child, ui->children())
			{
				if (child->isWidgetType())
				{
					QWidget* childWidget = qobject_cast<QWidget*>(child);
					if (childWidget)
					{
						qDebug() << childWidget->objectName() << childWidget->parent()->objectName();

						childWidget->hide();
					}
				}
			}

			if (this->layout() != nullptr)
			{
				QLayoutItem* item;
				while ((item = this->layout()->takeAt(0)) != nullptr)
				{
					qDebug() << "brisem" << item->widget()->objectName() << item->widget()->parent()->objectName();

					item->widget()->setVisible(false);
					delete item;
				}
				delete this->layout();
			}

			QLayout* postojeciLayout = this->layout();
			if (postojeciLayout)
			{
				delete postojeciLayout;
			}

			this->prikaziEdukativniTekst();
		});
	}
}

void Permutacije::pomoc()
{

	QDialog dijalog;
	dijalog.setFixedHeight(300);
	dijalog.setFixedWidth(400);
	dijalog.setStyleSheet("QDialog { font: bold 14px; background-color: "
						  "rgb(200,200,200); margin: 10px;}");

	QLabel* text = new QLabel("Zadatak je da poklopite sva slova. Svaki simbol različito "
							  "deluje na kombinaciju slova, i jedino ograničenje je da ne "
							  "možeš dva puta kliknuti isti simbol. Srećno!",
							  &dijalog);
	text->setAlignment(Qt::AlignCenter);
	text->setWordWrap(true);
	text->setStyleSheet("QLabel { font: bold 14px; color: #333; padding: 10px;}");
	QPushButton* ok = new QPushButton("OK", &dijalog);
	ok->setStyleSheet("QPushButton { min-width: 50px; min-height: 20px; margin-right: 10px; "
					  "background-color: #f0f0f0; border: 1px solid #dcdcdc; color: #333; "
					  "padding: 10px; border-radius: 21px; font: bold 14px;}"
					  "QPushButton:hover {background-color: #e0e0e0; border: 1px solid "
					  "#bcbcbc; }"
					  "QPushButton:pressed { background-color: #d0d0d0; border: 1px solid "
					  "#a0a0a0; }");
	QVBoxLayout* layout = new QVBoxLayout(&dijalog);

	layout->addWidget(text);

	layout->addWidget(ok);
	connect(ok, &QPushButton::clicked, &dijalog, &QDialog::accept);
	dijalog.exec();
}
